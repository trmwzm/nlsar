/*
** sarnlsar.c: Matlab interface of the non-local SAR filter
**
** This file is part of NL-SAR Toolbox version 0.8.
**
** Copyright Charles-Alban Deledalle (2016)
** Email charles-alban.deledalle@math.u-bordeaux.fr
**
** This software is a computer program whose purpose is to provide a
** suite of tools to manipulate SAR images.
**
** This software is governed by the CeCILL license under French law and
** abiding by the rules of distribution of free software. You can use,
** modify and/ or redistribute the software under the terms of the CeCILL
** license as circulated by CEA, CNRS and INRIA at the following URL
** "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided only
** with a limited warranty and the software's author, the holder of the
** economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards their
** requirements in conditions enabling the security of their systems and/or
** data to be ensured and, more generally, to use and operate it in the
** same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL license and that you accept its terms.
**
**
** Started on  Wed Jul 24 16:03:32 2013 Charles-Alban Deledalle
** Last update Tue Jul 19 18:44:48 2016 Charles-Alban Deledalle
*/

#ifdef __MINGW32__
typedef unsigned short char16_t;
#endif

#include <mex.h>
#include <string.h>
#include "data/sardata.h"
#include "data/fltdata.h"
#include "tools/sarerror.h"
#include "algos/nlsar/nlsar.h"

static void usage()
{
  char str[1024];
  sprintf(str, "usage: [sarout lookmap hWmap hPmap Smap alphamap] = sarnlsar(sarin, stats, verbose, h, rho)\n");
  mexErrMsgTxt(str);
}

#define isScalarValue(a) (mxIsNumeric(a) && !mxIsComplex(a) && mxGetM(a) == 1 && mxGetN(a) == 1)
#define isString(a) (mxIsChar(a) && mxGetM(a) == 1)

typedef union {
    double	d;
    void*	p;
} double_and_ptr ;

void mexFunction(int nlhs, mxArray *plhs[],
		 int nrhs, const mxArray *prhs[])
{
  int verbose = 1;
  float rho = 1.0;
  float h = 1.0;
  int M, N, D;
  sardata* input;
  sardata* output;
  sarsimstats* stats;
  fltdata* outlook = NULL;
  fltdata* outhW = NULL;
  fltdata* outhP = NULL;
  fltdata* outS = NULL;
  fltdata* outalpha = NULL;
  const mwSize* sizes;
  float* Pr;
  float* Pi;
  int i, j, k, l;

  if (nrhs < 2 || nrhs > 5 || nlhs > 7)
    {
      usage();
      return;
    }
  if (mxIsSingle(prhs[0]) != 1 || mxGetNumberOfDimensions(prhs[0]) != 4 ||
      !isScalarValue(prhs[1]) ||
      (nrhs > 2 && !isScalarValue(prhs[2])))
    {
      usage();
      return;
    }
  double_and_ptr s;
  s.d = (double) *mxGetPr(prhs[1]);
  stats = s.p;
  if (nrhs > 2) verbose = (int) *mxGetPr(prhs[2]);
  if (nrhs > 3) rho = (float) *mxGetPr(prhs[3]);
  if (nrhs > 4) h = (float) *mxGetPr(prhs[4]);

  // Load input image
  sizes = mxGetDimensions(prhs[0]);
  if (sizes[0] != sizes[1])
    {
      usage();
      return;
    }
  M = sizes[3];
  N = sizes[2];
  D = sizes[1];
  if (!(input = sardata_alloc_size(M, N, D)))
    {
      sarerror_msg_msg("Cannot allocate memory");
      mexErrMsgTxt(sarerror);
      return;
    }
  Pr = mxGetData(prhs[0]);
  Pi = mxGetImagData(prhs[0]);
  for (i = 0; i < M; ++i)
    for (j = 0; j < N; ++j)
      for (k = 0; k < D; ++k)
	for (l = 0; l < D; ++l)
	  if (mxIsComplex(prhs[0]))
	    SARDATA_ACCESS(input, i, j, k, l) =
	      Pr[(((i * N) + j) * D + k) * D + l]
	      + I * Pi[(((i * N) + j) * D + k) * D + l];
	  else
	    SARDATA_ACCESS(input, i, j, k, l) =
	      Pr[(((i * N) + j) * D + k) * D + l];

  // Filter image
  output = sardata_alloc();
  if (nlhs >= 2)
    outlook = fltdata_calloc_size(M, N, 1);
  if (nlhs >= 3)
    outhW = fltdata_calloc_size(M, N, 1);
  if (nlhs >= 4)
    outhP = fltdata_calloc_size(M, N, 1);
  if (nlhs >= 5)
    outS = fltdata_calloc_size(M, N, 1);
  if (nlhs >= 6)
    outalpha = fltdata_calloc_size(M, N, 1);

  output = sarnlsar(input, output, stats, 8, verbose, rho, h,
		    outlook, outhW, outhP, outS, outalpha);

  if (!output)
    {
      mexErrMsgTxt(sarerror);
      return;
    }
  // Output
  if (nlhs > 0)
    {
      mwSize sizes[4];
      M = output->M;
      N = output->N;
      D = output->D;
      sizes[0] = D;
      sizes[1] = D;
      sizes[2] = N;
      sizes[3] = M;
      plhs[0] = mxCreateNumericArray(4, sizes, mxSINGLE_CLASS, mxCOMPLEX);
      float* Pr = mxGetData(plhs[0]);
      float* Pi = mxGetImagData(plhs[0]);
      for (i = 0; i < M; ++i)
	for (j = 0; j < N; ++j)
	  for (k = 0; k < D; ++k)
	    for (l = 0; l < D; ++l)
	      {
		Pr[(((i * N) + j) * D + k) * D + l] = crealf(SARDATA_ACCESS(output, i, j, k, l));
		Pi[(((i * N) + j) * D + k) * D + l] = cimagf(SARDATA_ACCESS(output, i, j, k, l));
	      }
    }
  if (nlhs > 1)
    {
      mwSize sizes[2];
      M = outlook->M;
      N = outlook->N;
      sizes[0] = N;
      sizes[1] = M;
      plhs[1] = mxCreateNumericArray(2, sizes, mxSINGLE_CLASS, mxREAL);
      float* Pr = mxGetData(plhs[1]);
      for (i = 0; i < M; ++i)
	for (j = 0; j < N; ++j)
	  Pr[i * N + j] = FLTDATA_ACCESS(outlook, i, j, 0);
    }
  if (nlhs > 2)
    {
      mwSize sizes[2];
      M = outhW->M;
      N = outhW->N;
      sizes[0] = N;
      sizes[1] = M;
      plhs[2] = mxCreateNumericArray(2, sizes, mxSINGLE_CLASS, mxREAL);
      float* Pr = mxGetData(plhs[2]);
      for (i = 0; i < M; ++i)
	for (j = 0; j < N; ++j)
	  Pr[i * N + j] = FLTDATA_ACCESS(outhW, i, j, 0);
    }
  if (nlhs > 3)
    {
      mwSize sizes[2];
      M = outhP->M;
      N = outhP->N;
      sizes[0] = N;
      sizes[1] = M;
      plhs[3] = mxCreateNumericArray(2, sizes, mxSINGLE_CLASS, mxREAL);
      float* Pr = mxGetData(plhs[3]);
      for (i = 0; i < M; ++i)
	for (j = 0; j < N; ++j)
	  Pr[i * N + j] = FLTDATA_ACCESS(outhP, i, j, 0);
    }
  if (nlhs > 4)
    {
      mwSize sizes[2];
      M = outS->M;
      N = outS->N;
      sizes[0] = N;
      sizes[1] = M;
      plhs[4] = mxCreateNumericArray(2, sizes, mxSINGLE_CLASS, mxREAL);
      float* Pr = mxGetData(plhs[4]);
      for (i = 0; i < M; ++i)
	for (j = 0; j < N; ++j)
	  Pr[i * N + j] = FLTDATA_ACCESS(outS, i, j, 0);
    }
  if (nlhs > 5)
    {
      mwSize sizes[2];
      M = outalpha->M;
      N = outalpha->N;
      sizes[0] = N;
      sizes[1] = M;
      plhs[5] = mxCreateNumericArray(2, sizes, mxSINGLE_CLASS, mxREAL);
      float* Pr = mxGetData(plhs[5]);
      for (i = 0; i < M; ++i)
	for (j = 0; j < N; ++j)
	  Pr[i * N + j] = FLTDATA_ACCESS(outalpha, i, j, 0);
    }
  sardata_free(input);
  sardata_free(output);
  if (outlook)
    fltdata_free(outlook);
  if (outhW)
    fltdata_free(outhW);
  if (outhP)
    fltdata_free(outhP);
  if (outS)
    fltdata_free(outS);
  if (outalpha)
    fltdata_free(outalpha);
}
