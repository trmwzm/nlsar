#ifdef __MINGW32__
typedef unsigned short char16_t;
#endif

#include <mex.h>
#include <string.h>
#include "data/sardata.h"
#include "data/fltdata.h"
#include "tools/sarerror.h"
#include "algos/nlsar/sarsimstats.h"

static void usage()
{
  char str[1024];
  sprintf(str, "usage: sarnlstats_free(stats)\n");
  mexErrMsgTxt(str);
}

#define isScalarValue(a) (mxIsNumeric(a) && !mxIsComplex(a) && mxGetM(a) == 1 && mxGetN(a) == 1)
#define isString(a) (mxIsChar(a) && mxGetM(a) == 1)

typedef union {
    double	d;
    void*	p;
} double_and_ptr ;

void mexFunction(int nlhs, mxArray *plhs[],
		 int nrhs, const mxArray *prhs[])
{
  plhs = plhs;

  if (nrhs < 1 || nrhs > 1 || nlhs > 0)
    {
      usage();
      return;
    }
  if (!isScalarValue(prhs[0]))
    {
      usage();
      return;
    }
  double_and_ptr s;
  s.d = (double) *mxGetPr(prhs[0]);

  sarnlstats_free(s.p);
}
