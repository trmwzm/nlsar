function ima_fil = sarguinlsar(filename)
%SARGUINLSAR        An experimental GUI interface for NL-SAR
%%
%% sarguinlsar.m: An experimental GUI interface for NL-SAR
%%
%% This file is part of NL-SAR Toolbox version 0.8.
%%
%% Copyright Charles-Alban Deledalle (2016)
%% Email charles-alban.deledalle@math.u-bordeaux.fr
%%
%% This software is a computer program whose purpose is to provide a
%% suite of tools to manipulate SAR images.
%%
%% This software is governed by the CeCILL license under French law and
%% abiding by the rules of distribution of free software. You can use,
%% modify and/ or redistribute the software under the terms of the CeCILL
%% license as circulated by CEA, CNRS and INRIA at the following URL
%% "http://www.cecill.info".
%%
%% As a counterpart to the access to the source code and rights to copy,
%% modify and redistribute granted by the license, users are provided only
%% with a limited warranty and the software's author, the holder of the
%% economic rights, and the successive licensors have only limited
%% liability.
%%
%% In this respect, the user's attention is drawn to the risks associated
%% with loading, using, modifying and/or developing or reproducing the
%% software by the user in light of its specific status of free software,
%% that may mean that it is complicated to manipulate, and that also
%% therefore means that it is reserved for developers and experienced
%% professionals having in-depth computer knowledge. Users are therefore
%% encouraged to load and test the software's suitability as regards their
%% requirements in conditions enabling the security of their systems and/or
%% data to be ensured and, more generally, to use and operate it in the
%% same conditions as regards security.
%%
%% The fact that you are presently reading this means that you have had
%% knowledge of the CeCILL license and that you accept its terms.
%%
%%
%% Started on  Wed Jul 24 16:00:24 2013 Charles-Alban Deledalle
%% Last update Fri Jul 15 15:41:52 2016 Charles-Alban Deledalle

    if nargin < 1
        [fn, pn] = uigetfile('*.rat', 'Open as');
        if fn == 0
            return
        end
        filename = [ pn '/' fn ];
    else
        pn = pwd;
    end
    if ischar(filename) && ~exist(filename)
        error(sprintf('%s: no such file or directory'));
    end
    stats = 0;
    noise = [];
    ima_nse = [];
    ima_fil = [];
    ima_look = [];
    ima_hW = [];
    ima_hP = [];
    ima_S = [];
    ima_alpha = [];
    ima_weights = [];
    ima_weights_signal = [];
    var = 'enl';
    weight = 'weight';
    hWmax = 0;

    h = openfig('sarguinlsar.fig', 'new', 'invisible');
    set(h,'HitTest','off')
    set(gcf, 'units','normalized','outerposition',[0 0 1 1]);
    g = guihandles(h);

    set(gcf, 'CurrentAxes', g.axes3); axis off; axis image
    set(gcf, 'CurrentAxes', g.axes4); axis off; axis image
    set(gcf, 'CurrentAxes', g.axes5); axis off; axis image
    set(gcf, 'toolbar',' figure');

    openimage(filename)

    set(g.pushbutton1, 'Callback', @(gco, event, dumy) callback('updatenoise()'));
    set(g.pushbutton2, 'Callback', @(gco, event, dumy) callback('runnlsar(''default'')'));
    set(g.pushbutton3, 'Callback', @(gco, event, dumy) callback('save()'));
    set(g.pushbutton4, 'Callback', @(gco, event, dumy) callback('opennoise()'));
    set(g.pushbutton5, 'Callback', @(gco, event, dumy) callback('openimage()'));
    set(g.pushbutton8, 'Callback', @(gco, event, dumy) callback('runnlsar(''preview'')'));
    set(g.pushbutton9, 'Callback', @(gco, event, dumy) callback('saveparam()'));
    set(g.pushbutton10, 'Callback', @(gco, event, dumy) callback('saveweight()'));
    set(g.slider1, 'Callback',     @(gco, event, dumy) callback('slidevar()'));
    set(g.slider2, 'Callback',     @(gco, event, dumy) callback('slide2var()'));
    set(g.slider3, 'Callback',     @(gco, event, dumy) callback('slide3var()'));
    set(g.slider4, 'Callback',     @(gco, event, dumy) callback('slide2var()'));
    set(g.slider5, 'Callback',     @(gco, event, dumy) callback('slide3var()'));
    set(h, 'Visible', 'on');
    waitfor(h);
    if stats ~= 0
        sarnlstats_free(stats);
    end
    if nargout == 0
        clear ima_fil;
    end

    function callback(string)
        disable();
        eval(string);
        enable();
    end

    function disable()
        fng = fieldnames(g);
        for k = 1:length(fng)
            if isprop(getfield(g, fng{k}), 'Enable')
                set(getfield(g, fng{k}), 'Enable', 'off')
            end
        end
    end

    function enable()
        fng = fieldnames(g);
        for k = 1:length(fng)
            if isprop(getfield(g, fng{k}), 'Enable')
                set(getfield(g, fng{k}), 'Enable', 'on')
            end
        end
    end

    function updatenoise()
        noise = sarguihomogeneous(ima_nse);

        set(gcf, 'CurrentAxes', g.axes2);
        sarshow(noise);
        xlim([0 size(noise, 3)]+0.5);
        ylim([0 size(noise, 4)]+0.5);
    end

    function runnlsar(mode)
        hfil = double(str2num(get(g.edit1, 'String')));
        switch mode
          case 'preview'
            hWmin = get(g.popupmenu7, 'Value');
            hPmin = get(g.popupmenu8, 'Value')-1;
            hWmax = get(g.popupmenu3, 'Value');
            hPmax = get(g.popupmenu4, 'Value')-1;
          otherwise
            hWmin = get(g.popupmenu5, 'Value');
            hPmin = get(g.popupmenu6, 'Value')-1;
            hWmax = get(g.popupmenu1, 'Value');
            hPmax = get(g.popupmenu2, 'Value')-1;
        end
        if stats ~= 0
            sarnlstats_free(stats);
        end

        stats = sarnlstats(noise, 1, 1, hWmin, hWmax, hPmin, hPmax);
        [ima_fil, ima_look, ima_hW, ima_hP, ima_S, ima_alpha] = ...
            sarnlsar(ima_nse, stats, 1, hfil);
        figure(h)

        set(gcf, 'CurrentAxes', g.axes3);
        xl = get(g.axes1, 'XLim');
        yl = get(g.axes1, 'YLim');
        sarshow(ima_fil);
        set(g.axes3, 'XLim', xl);
        set(g.axes3, 'YLim', yl);

        set(get(g.axes3,'Children'), 'ButtonDownFcn', ...
                          @(gco, event) getweights());

        set(g.slider3, 'Value', 3/12);
        set(g.slider5, 'Value', 0.7/2);

        var = 'enl';
        set(g.slider1, 'Value', sqrt(2/(2*hWmax+1)));
        slidevar();

        linkaxes([g.axes1 g.axes3 g.axes4])
    end

    function changevar()
        switch var
          case 'enl'
            var = 'hW';
            set(g.slider1, 'Value', 0.99);
          case 'hW'
            var = 'hP';
            set(g.slider1, 'Value', 0.99);
          case 'hP'
            var = 'S';
            set(g.slider1, 'Value', 0.99);
          case 'S'
            var = 'alpha';
            set(g.slider1, 'Value', 1);
          case 'alpha'
            var = 'enl';
            set(g.slider1, 'Value', sqrt(2/(2*hWmax+1)));
        end
        slidevar()
    end

    function slidevar()
        set(gcf, 'CurrentAxes', g.axes4);
        xl = get(g.axes1, 'XLim');
        yl = get(g.axes1, 'YLim');
        switch var
          case 'enl'
            set(g.text10, 'String', 'Local ENL');
            imagesc(sqrt(ima_look'));
            caxis([0 get(g.slider1, 'Value')*max(sqrt(ima_look(:)))]);
          case 'hW'
            set(g.text10, 'String', 'Local seach window size');
            imagesc(ima_hW');
            caxis([0 get(g.slider1, 'Value')*max(ima_hW(:))]);
          case 'hP'
            set(g.text10, 'String', 'Local patch size');
            imagesc(ima_hP');
            caxis([0 get(g.slider1, 'Value')*max(ima_hP(:))]);
          case 'S'
            set(g.text10, 'String', 'Local scale');
            imagesc(ima_S');
            caxis([0 get(g.slider1, 'Value')*max(ima_S(:))]);
          case 'alpha'
            set(g.text10, 'String', 'Local alpha');
            imagesc(ima_alpha');
            caxis([0 get(g.slider1, 'Value')]);
        end
        colormap(gray);
        axis image;
        axis off;
        set(g.axes4, 'XLim', xl);
        set(g.axes4, 'YLim', yl);

        set(get(gca,'Children'), 'ButtonDownFcn', @(x, y) changevar());
    end

    function openimage(filename)
        if nargin < 1
            [fn, pnn] = uigetfile('*.rat', 'Open as', pn);
            if pnn == 0
                return
            end
            pn = pnn;
            filename = [ pn '/' fn ];
        end
        if ischar(filename)
            if ~exist(filename)
                error(sprintf('%s: no such file or directory'));
            end

            [M, N, D] = sarinfo(filename);
            xoffset = double(floor((M - min(M, 512)) / 2));
            yoffset = double(floor((N - min(N, 512)) / 2));
            M = double(min(M, 512));
            N = double(min(N, 512));
            ima_nse = sarread(filename, double(xoffset), double(yoffset), ...
                              double(M), double(N));
        else
            ima_nse = filename;
            [D, D, N, M] = size(ima_nse);
            xoffset = double(floor((M - min(M, 512)) / 2));
            yoffset = double(floor((N - min(N, 512)) / 2));
            M = double(min(M, 512));
            N = double(min(N, 512));
            ima_nse = ima_nse(:, :, ...
                              yoffset + (1:N), ...
                              xoffset + (1:M));
        end

        W = 48;
        hom = sarhomogeneous(ima_nse, W);
        [~, idx] = sort(hom(:), 'descend');
        [i, j] = ind2sub([N, M], idx(1));
        noise = ima_nse(:, :, ...
                        mod(i-1 + (1:W) - 1, N) + 1, ...
                        mod(j-1 + (1:W) - 1, M) + 1);

        linkaxes([g.axes1 g.axes3 g.axes4], 'off')

        set(gcf, 'CurrentAxes', g.axes1);
        sarshow(ima_nse);
        xlim([0 size(ima_nse, 3)]+0.5);
        ylim([0 size(ima_nse, 4)]+0.5);

        set(gcf, 'CurrentAxes', g.axes2);
        sarshow(noise);
        xlim([0 size(noise, 3)]+0.5);
        ylim([0 size(noise, 4)]+0.5);

        delete(allchild(g.axes3))
        delete(allchild(g.axes4))
        delete(allchild(g.axes5))

        set(g.slider2, 'Value', 3/12);
        set(g.slider4, 'Value', 0.7/2);
        slide2var()

        set(gcf, 'CurrentAxes', g.axes3); axis off; axis image
        set(gcf, 'CurrentAxes', g.axes4); axis off; axis image
        set(gcf, 'CurrentAxes', g.axes5); axis off; axis image

    end

    function opennoise(filename)
        if nargin < 1
            [fn, pnn] = uigetfile('*.rat', 'Open as', pn);
            if fn == 0
                return
            end
            pn = pnn;
            filename = [ pn '/' fn ];
        end
        if ~exist(filename)
            error(sprintf('%s: no such file or directory'));
        end

        noise = sarread(filename);

        set(gcf, 'CurrentAxes', g.axes2);
        sarshow(noise);
        xlim([0 size(noise, 3)]+0.5);
        ylim([0 size(noise, 4)]+0.5);
    end

    function getweights()
        point1 = get(g.axes3, 'CurrentPoint');
        x = round(point1(1, 1));
        y = round(point1(1, 2));
        a = double(ima_hW(x, y));
        b = double(ima_hP(x, y));
        c = double(ima_S(x, y));
        d = double(ima_alpha(x, y));
        ima_weights = sargetweights(ima_nse, stats, y-1, x-1, ...
                                    a, b, c, double(0));
        [~, ~, N, M] = size(ima_nse);
        ima_weights_signal = ima_nse(:,:, ...
                                     mod(x+(-a:a) - 1, N) + 1, ...
                                     mod(y+(-a:a) - 1, M) + 1);
        %ima_weights(a+2, a+2) = 0;
        %if max(ima_weights(:)) == 0
        %    ima_weights(a+2, a+2) = 1;
        %else
        %    ima_weights(a+2, a+2) = max(ima_weights(:));
        %end
        weight = 'weight';
        switchweights();
        set(get(g.axes5,'Children'), 'ButtonDownFcn', ...
                          @(gco, event) switchweights());

    end

    function switchweights()
        set(gcf, 'CurrentAxes', g.axes5);
        switch weight
          case 'weight'
            imagesc(ima_weights');
            %caxis([0 1]);
            colormap(gray);
            axis image;
            axis off;
            weight = 'signal';
          case 'signal'
            sarshow(ima_weights_signal);
            xlim([0 size(ima_weights_signal, 3)]+0.5);
            ylim([0 size(ima_weights_signal, 4)]+0.5);
            weight = 'weight';
        end
        set(get(g.axes5,'Children'), 'ButtonDownFcn', ...
                          @(gco, event) switchweights());
    end

    function slide2var()
        set(gcf, 'CurrentAxes', g.axes1);
        xl = get(g.axes1, 'XLim');
        yl = get(g.axes1, 'YLim');
        sarshow(ima_nse, get(g.slider2, 'Value')*12, get(g.slider4, 'Value')*2);
        set(g.axes1, 'XLim', xl);
        set(g.axes1, 'YLim', yl);
    end

    function slide3var()
        set(gcf, 'CurrentAxes', g.axes3);
        xl = get(g.axes3, 'XLim');
        yl = get(g.axes3, 'YLim');
        sarshow(ima_fil, get(g.slider3, 'Value')*12, get(g.slider5, 'Value')*2);
        set(g.axes3, 'XLim', xl);
        set(g.axes3, 'YLim', yl);

        set(get(g.axes3,'Children'), 'ButtonDownFcn', ...
                          @(gco, event) getweights());
    end

    function save()
        if isempty(ima_fil)
            return
        end
        [fn, pnn] = uiputfile('*.rat', 'Save as', [pn '/' 'result.rat']);
        if fn == 0
            return
        end
        pn = pnn;
        sarwrite(ima_fil, [pn '/' fn]);
        sarwrite(ima_nse, [pn '/' fn(1:end-4) '.noisy.rat']);
        sarwrite(noise, [pn '/' fn(1:end-4) '.noise.rat']);
        sarnlstats_write(stats, [pn '/' fn(1:end-4) '.noise.stats']);
    end

    function saveparam()
        if isempty(ima_fil)
            return
        end
        [fn, pnn] = uiputfile('*.png', 'Save as', [pn '/' 'result.png']);
        if fn == 0
            return
        end
        pn = pnn;
        switch var
          case 'enl'
            imwrite(sqrt(ima_look') / ...
                    (get(g.slider1, 'Value')*max(sqrt(ima_look(:)))), ...
                    [pn '/' fn]);
          case 'hW'
            imwrite(ima_hW' / ...
                    (get(g.slider1, 'Value')*max(ima_hW(:))), ...
                    [pn '/' fn]);
          case 'hP'
            imwrite(ima_hP' / ...
                    (get(g.slider1, 'Value')*max(ima_hP(:))), ...
                    [pn '/' fn]);
          case 'S'
            imwrite(ima_S' / ...
                    (get(g.slider1, 'Value')*max(ima_S(:))), ...
                    [pn '/' fn]);
          case 'alpha'
            imwrite(ima_alpha' / ...
                    (get(g.slider1, 'Value')), ...
                    [pn '/' fn]);
        end
    end

    function saveweight()

        if isempty(ima_fil)
            return
        end
        [fn, pnn] = uiputfile('*.png', 'Save as', [pn '/' 'result.png']);
        if fn == 0
            return
        end
        pn = pnn;
        switch weight
          case 'signal'
            imwrite(ima_weights' / max(ima_weights(:)), [pn '/' fn]);
          case 'weight'
            sar2png(ima_weights_signal, [pn '/' fn]);
        end
    end
end
