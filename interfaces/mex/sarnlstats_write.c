#ifdef __MINGW32__
typedef unsigned short char16_t;
#endif

#include <mex.h>
#include <string.h>
#include "data/sardata.h"
#include "data/fltdata.h"
#include "tools/sarerror.h"
#include "algos/nlsar/sarsimstats.h"

static void usage()
{
  char str[1024];
  sprintf(str, "usage: sarnlstats_write(stats, filename)\n");
  mexErrMsgTxt(str);
}

#define isScalarValue(a) (mxIsNumeric(a) && !mxIsComplex(a) && mxGetM(a) == 1 && mxGetN(a) == 1)
#define isString(a) (mxIsChar(a) && mxGetM(a) == 1)

typedef union {
    double	d;
    void*	p;
} double_and_ptr ;

void mexFunction(int nlhs, mxArray *plhs[],
		 int nrhs, const mxArray *prhs[])
{
  char* filename;

  if (nrhs < 2 || nrhs > 2 || nlhs > 0)
    {
      usage();
      return;
    }
  if (!isScalarValue(prhs[0]) || !isString(prhs[1]))
    {
      usage();
      return;
    }
  plhs = plhs;

  double_and_ptr s;
  s.d = (double) *mxGetPr(prhs[0]);
  filename = mxArrayToString(prhs[1]);

  if (!(sarnlstats_write(s.p, filename)))
    {
      sarerror_msg_msg("Cannot create file %s", filename);
      mexErrMsgTxt(sarerror);
      return;
    }
}
