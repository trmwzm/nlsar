
#ifdef __MINGW32__
typedef unsigned short char16_t;
#endif

#include <mex.h>
#include <string.h>
#include "data/sardata.h"
#include "data/fltdata.h"
#include "tools/sarerror.h"
#include "algos/nlsar/sarsimstats.h"

static void usage()
{
  char str[1024];
  sprintf(str, "usage: stats = sarnlstats(sarin, L, [verbose, hWmin=1, hWmax=12, hPmin=0, hPmax=5])\n");
  mexErrMsgTxt(str);
}

#define isScalarValue(a) (mxIsNumeric(a) && !mxIsComplex(a) && mxGetM(a) == 1 && mxGetN(a) == 1)
#define isString(a) (mxIsChar(a) && mxGetM(a) == 1)

typedef union {
    double	d;
    void*	p;
} double_and_ptr ;

void mexFunction(int nlhs, mxArray *plhs[],
		 int nrhs, const mxArray *prhs[])
{
  float L = 1;
  int verbose = 1, hWmin=1, hWmax = 12, hPmin=0, hPmax = 5;
  int M, N, D;
  sardata* input;
  sarsimstats* stats;
  const mwSize* sizes;
  float* Pr;
  float* Pi;
  int i, j, k, l;

  if (nrhs < 2 || nrhs > 7 || nlhs > 1)
    {
      usage();
      return;
    }
  if (mxIsSingle(prhs[0]) != 1 || mxGetNumberOfDimensions(prhs[0]) != 4 ||
      !isScalarValue(prhs[1]) ||
      (nrhs > 2 && !isScalarValue(prhs[2])) ||
      (nrhs > 3 && !isScalarValue(prhs[3])) ||
      (nrhs > 4 && !isScalarValue(prhs[4])) ||
      (nrhs > 5 && !isScalarValue(prhs[5])) ||
      (nrhs > 6 && !isScalarValue(prhs[6])))
    {
      usage();
      return;
    }
  L = (float) *mxGetPr(prhs[1]);
  if (nrhs > 2) verbose = (int) *mxGetPr(prhs[2]);
  if (nrhs > 3) hWmin = (int) *mxGetPr(prhs[3]);
  if (nrhs > 4) hWmax = (int) *mxGetPr(prhs[4]);
  if (nrhs > 5) hPmin = (int) *mxGetPr(prhs[5]);
  if (nrhs > 6) hPmax = (int) *mxGetPr(prhs[6]);

  // Load input image
  sizes = mxGetDimensions(prhs[0]);
  if (sizes[0] != sizes[1])
    {
      usage();
      return;
    }
  M = sizes[3];
  N = sizes[2];
  D = sizes[1];
  if (!(input = sardata_alloc_size(M, N, D)))
    {
      sarerror_msg_msg("Cannot allocate memory");
      mexErrMsgTxt(sarerror);
      return;
    }
  Pr = mxGetData(prhs[0]);
  Pi = mxGetImagData(prhs[0]);
  for (i = 0; i < M; ++i)
    for (j = 0; j < N; ++j)
      for (k = 0; k < D; ++k)
	for (l = 0; l < D; ++l)
	  if (mxIsComplex(prhs[0]))
	    SARDATA_ACCESS(input, i, j, k, l) =
	      Pr[(((i * N) + j) * D + k) * D + l]
	      + I * Pi[(((i * N) + j) * D + k) * D + l];
	  else
	    SARDATA_ACCESS(input, i, j, k, l) =
	      Pr[(((i * N) + j) * D + k) * D + l];

  if (!(stats = sarnlstats(input, L, 5, verbose,
			   hWmin, hWmax, hPmin, hPmax)))
    {
      mexErrMsgTxt(sarerror);
      return;
    }
  sardata_free(input);

  mwSize sizes_out = 1;
  plhs[0] = mxCreateNumericArray(1, &sizes_out, mxDOUBLE_CLASS, mxREAL);
  double* p = mxGetData(plhs[0]);
  double_and_ptr s;
  s.p = stats;
  *p = s.d;
}
