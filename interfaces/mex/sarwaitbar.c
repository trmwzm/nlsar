/*
** sarwaitbar.c: wrapper for waitbar in Matlab interface
**
** This file is part of NL-SAR Toolbox version 0.8.
**
** Includes waitmex code:
**   Copyright (c) 2014, Timothy A. Davis
**
**   Redistribution and use in source and binary forms, with or without
**   modification, are permitted provided that the following conditions are
**   met:
**
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in
**       the documentation and/or other materials provided with the distribution
**
** Copyright Charles-Alban Deledalle (2016)
** Email charles-alban.deledalle@math.u-bordeaux.fr
**
** This software is a computer program whose purpose is to provide a
** suite of tools to manipulate SAR images.
**
** This software is governed by the CeCILL license under French law and
** abiding by the rules of distribution of free software. You can use,
** modify and/ or redistribute the software under the terms of the CeCILL
** license as circulated by CEA, CNRS and INRIA at the following URL
** "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided only
** with a limited warranty and the software's author, the holder of the
** economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards their
** requirements in conditions enabling the security of their systems and/or
** data to be ensured and, more generally, to use and operate it in the
** same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL license and that you accept its terms.
**
**
** Started on  Wed Jul 24 16:03:09 2013 Charles-Alban Deledalle
** Last update Tue Jul 19 17:44:52 2016 Charles-Alban Deledalle
*/

#ifdef __MINGW32__
typedef unsigned short char16_t;
#endif

#include <mex.h>
#include <matrix.h>
#include <time.h>
#ifdef OMP
# include <omp.h>
#endif //!OMP
#include "tools/sarwaitbar.h"

// Waitmex code -----------------------------------------------------------

typedef struct
{
    mxArray *inputs[3];        /* waitbar inputs */
    mxArray *outputs[2];       /* waitbar outputs */
    mxArray *handle;           /* handle from waitbar */
    mxArray *fraction;         /* fraction from 0 to 1 (a scalar) */
    mxArray *message;          /* waitbar message */

} waitbar;

waitbar *waitbar_create(double fraction, char *message)
{
  mxArray* error;
  waitbar *h;
  mxArray *opt;

  h = mxMalloc(sizeof(waitbar));
  h->fraction = mxCreateDoubleScalar(fraction);
  h->message = mxCreateString(message);

  /* h = waitbar(fraction, message); */
  h->inputs[0] = h->fraction;
  h->inputs[1] = h->message;
  error = mexCallMATLABWithTrap(1, h->outputs, 2, h->inputs, "waitbar");
  /* set(h, 'WindowStyle', 'modal'); */
  opt = mxCreateString("modal");
  mxSetProperty(h->outputs[0], 0, "WindowStyle", opt);
  mxDestroyArray(opt);
  mexCallMATLABWithTrap(0, NULL, 0, NULL, "drawnow");
  if (error)
    return NULL;

  /* save the MATLAB handle h in the waitbar struct */
  h->handle = h->outputs[0];

  return h;
}

void waitbar_update(double fraction, waitbar *h, char *message)
{
  if (h == NULL) return;                 /* nothing to do */
  (* mxGetPr(h->fraction)) = fraction;  /* update the fraction */
  h->inputs[0] = h->fraction;           /* define the inputs x and h */
  h->inputs[1] = h->handle;

  if (message == NULL)
    {
      /* use the existing message; waitbar(x,h) in MATLAB */
      mexCallMATLABWithTrap(0, h->outputs, 2, h->inputs, "waitbar");
    }
  else
    {
      /* define a new message; waitbar(x,h,message) in MATLAB */
      mxDestroyArray(h->message);
      h->message = mxCreateString(message);
      h->inputs[2] = h->message;
      mexCallMATLABWithTrap(0, h->outputs, 3, h->inputs, "waitbar");
    }
  mexCallMATLABWithTrap(0, NULL, 0, NULL, "drawnow");
}

void waitbar_destroy(waitbar *h)
{
  if (h == NULL) return;             /* nothing to do */
  h->inputs[0] = h->handle;
  mxDestroyArray(h->fraction);      /* free the internal mxArrays */
  mxDestroyArray(h->message);
  mexCallMATLABWithTrap(0, NULL, 1, h->inputs, "close");
  mxDestroyArray(h->handle);
  mxFree(h);
}

// End of waitmex code ----------------------------------------------------

typedef struct
{
  time_t	init_time;
  waitbar*	hwait;
} time_and_waitbar;

void* sarwaitbar_matlab_open(void)
{
  time_and_waitbar* h;
#ifdef OMP
  if (omp_get_thread_num() != 0)
    return NULL;
#endif //!OMP
  h = malloc(sizeof(time_and_waitbar));
  h->init_time = time(NULL);
  h->hwait = waitbar_create(0, "Please wait (0%)");
  return h;
}

int sarwaitbar_matlab_update(void* handle, int percent)
{
  float spent_time = time(NULL) - ((time_and_waitbar*) handle)->init_time;
  float remaining_time = spent_time / percent * (100. - percent);
  char msg[1024];
#ifdef OMP
  if (omp_get_thread_num() != 0)
    return 1;
#endif //!OMP
  if (spent_time > 7)
    sprintf(msg, "Please wait (%d%%, remaining %d secs)", percent, (int) remaining_time);
  else
    sprintf(msg, "Please wait (%d%%)", percent);
  waitbar_update(((double) percent) / 100., ((time_and_waitbar*) handle)->hwait, msg);
  return 0;
}

int sarwaitbar_matlab_close(void* handle)
{
#ifdef OMP
  if (omp_get_thread_num() != 0)
    return 1;
#endif //!OMP
  waitbar_destroy(((time_and_waitbar*) handle)->hwait);
  free(handle);
  return 0;
}

void* (*sarwaitbar_open)(void) = &sarwaitbar_matlab_open;
int   (*sarwaitbar_update)(void*, int percent) = &sarwaitbar_matlab_update;
int   (*sarwaitbar_close)(void*) = &sarwaitbar_matlab_close;
