#ifdef __MINGW32__
typedef unsigned short char16_t;
#endif

#include <mex.h>
#include <string.h>
#include "data/sardata.h"
#include "data/fltdata.h"
#include "tools/sarerror.h"
#include "algos/nlsar/sarsimstats.h"

static void usage()
{
  char str[1024];
  sprintf(str, "usage: stats = sarnlstats_read(filename)\n");
  mexErrMsgTxt(str);
}

#define isScalarValue(a) (mxIsNumeric(a) && !mxIsComplex(a) && mxGetM(a) == 1 && mxGetN(a) == 1)
#define isString(a) (mxIsChar(a) && mxGetM(a) == 1)

typedef union {
    double	d;
    void*	p;
} double_and_ptr ;

void mexFunction(int nlhs, mxArray *plhs[],
		 int nrhs, const mxArray *prhs[])
{
  sarsimstats* stats;
  char* filename;

  if (nrhs < 1 || nrhs > 1 || nlhs > 1)
    {
      usage();
      return;
    }
  if (!isString(prhs[0]))
    {
      usage();
      return;
    }
  filename = mxArrayToString(prhs[0]);

  if (!(stats = sarnlstats_read(filename)))
    {
      mexErrMsgTxt(sarerror);
      return;
    }

  mwSize sizes_out = 1;
  plhs[0] = mxCreateNumericArray(1, &sizes_out, mxDOUBLE_CLASS, mxREAL);
  double* p = mxGetData(plhs[0]);
  double_and_ptr s;
  s.p = stats;
  *p = s.d;
}
