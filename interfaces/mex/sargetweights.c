/*
** sarnlsar.c: Matlab interface of the non-local SAR filter
**
** This file is part of NL-SAR Toolbox version 0.8.
**
** Copyright Charles-Alban Deledalle (2016)
** Email charles-alban.deledalle@math.u-bordeaux.fr
**
** This software is a computer program whose purpose is to provide a
** suite of tools to manipulate SAR images.
**
** This software is governed by the CeCILL license under French law and
** abiding by the rules of distribution of free software. You can use,
** modify and/ or redistribute the software under the terms of the CeCILL
** license as circulated by CEA, CNRS and INRIA at the following URL
** "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided only
** with a limited warranty and the software's author, the holder of the
** economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards their
** requirements in conditions enabling the security of their systems and/or
** data to be ensured and, more generally, to use and operate it in the
** same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL license and that you accept its terms.
**
**
** Started on  Wed Jul 24 16:03:32 2013 Charles-Alban Deledalle
** Last update Wed Jul 20 23:05:14 2016 Charles-Alban Deledalle
*/

#ifdef __MINGW32__
typedef unsigned short char16_t;
#endif

#include <mex.h>
#include <string.h>
#include "data/sardata.h"
#include "data/fltdata.h"
#include "tools/sarerror.h"
#include "algos/nlsar/nlsar.h"

static void usage()
{
  char str[1024];
  sprintf(str, "usage: [weights] = sargetweights(sarin, stats, x, y, hW, hP, S, alpha)\n");
  mexErrMsgTxt(str);
}

#define isScalarValue(a) (mxIsNumeric(a) && !mxIsComplex(a) && mxGetM(a) == 1 && mxGetN(a) == 1)
#define isString(a) (mxIsChar(a) && mxGetM(a) == 1)

typedef union {
    double	d;
    void*	p;
} double_and_ptr ;

void mexFunction(int nlhs, mxArray *plhs[],
		 int nrhs, const mxArray *prhs[])
{
  int hW, hP, S;
  int M, N, D;
  sardata* input;
  fltdata* outweights = NULL;
  sarsimstats* stats;
  const mwSize* sizes;
  float* Pr;
  float* Pi;
  int x, y, i, j, k, l;
  float alpha;

  if (nrhs < 8 || nrhs > 8 || nlhs > 1)
    {
      usage();
      return;
    }
  if (mxIsSingle(prhs[0]) != 1 || mxGetNumberOfDimensions(prhs[0]) != 4 ||
      !isScalarValue(prhs[1]) ||
      !isScalarValue(prhs[2]) ||
      !isScalarValue(prhs[3]) ||
      !isScalarValue(prhs[4]) ||
      !isScalarValue(prhs[5]) ||
      !isScalarValue(prhs[6]) ||
      !isScalarValue(prhs[7]))
    {
      usage();
      return;
    }

  double_and_ptr s;
  s.d = (double) *mxGetPr(prhs[1]);
  stats = s.p;

  x  = (int)   *mxGetPr(prhs[2]);
  y  = (int)   *mxGetPr(prhs[3]);
  hW = (int)   *mxGetPr(prhs[4]);
  hP = (int)   *mxGetPr(prhs[5]);
  S  =  (int)   *mxGetPr(prhs[6]);
  alpha = (float)   *mxGetPr(prhs[7]);

  // Load input image
  sizes = mxGetDimensions(prhs[0]);
  if (sizes[0] != sizes[1])
    {
      usage();
      return;
    }
  M = sizes[3];
  N = sizes[2];
  D = sizes[1];
  input = sardata_alloc_size(M, N, D);
  if (!input)
    {
      sarerror_msg_msg("Cannot allocate memory");
      mexErrMsgTxt(sarerror);
      return;
    }
  Pr = mxGetData(prhs[0]);
  Pi = mxGetImagData(prhs[0]);
  for (i = 0; i < M; ++i)
    for (j = 0; j < N; ++j)
      for (k = 0; k < D; ++k)
	for (l = 0; l < D; ++l)
	  if (mxIsComplex(prhs[0]))
	    SARDATA_ACCESS(input, i, j, k, l) =
	      Pr[(((i * N) + j) * D + k) * D + l]
	      + I * Pi[(((i * N) + j) * D + k) * D + l];
	  else
	    SARDATA_ACCESS(input, i, j, k, l) =
	      Pr[(((i * N) + j) * D + k) * D + l];

  // Compute wieghts
  outweights = fltdata_alloc();
  outweights = sargetweights(input, outweights, stats, x, y, hW, hP, S, alpha);

  if (!outweights)
    {
      mexErrMsgTxt(sarerror);
      return;
    }
  // Outweights
  if (nlhs > 0)
    {
      mwSize sizes[2];
      M = outweights->M;
      N = outweights->N;
      sizes[0] = N;
      sizes[1] = M;
      plhs[0] = mxCreateNumericArray(2, sizes, mxSINGLE_CLASS, mxREAL);
      float* Pr = mxGetData(plhs[0]);
      for (i = 0; i < M; ++i)
	for (j = 0; j < N; ++j)
	  Pr[i * N + j] = FLTDATA_ACCESS(outweights, i, j, 0);
    }
  sardata_free(input);
  fltdata_free(outweights);
}
