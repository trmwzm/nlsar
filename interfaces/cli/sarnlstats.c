/*
** sarnlsar_precomp.c: CLI for the precomputation NL-SAR filter
**
** This file is part of NL-SAR Toolbox version 0.8.
**
** Copyright Charles-Alban Deledalle (2016)
** Email charles-alban.deledalle@math.u-bordeaux.fr
**
** This software is a computer program whose purpose is to provide a
** suite of tools to manipulate SAR images.
**
** This software is governed by the CeCILL license under French law and
** abiding by the rules of distribution of free software. You can use,
** modify and/ or redistribute the software under the terms of the CeCILL
** license as circulated by CEA, CNRS and INRIA at the following URL
** "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided only
** with a limited warranty and the software's author, the holder of the
** economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards their
** requirements in conditions enabling the security of their systems and/or
** data to be ensured and, more generally, to use and operate it in the
** same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL license and that you accept its terms.
**
**
** Started on  Wed Jul 24 14:44:53 2013 Charles-Alban Deledalle
** Last update Wed Jul 22 14:04:51 2015 Charles-Alban Deledalle
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "tools/sarprintf.h"
#include "tools/sarerror.h"
#include "data/sardata.h"
#include "data/fltdata.h"
#include "data/iosar.h"
#include "data/ionetpbm.h"
#include "algos/nlsar/nlsar.h"

static int usage(const char* argv0)
{
  fprintf(stderr, "usage: %s filein fileout L [verbose hWmin hWmax hPmin hPmax simfunc]\n\n", argv0);
  fprintf(stderr, "\t L \t\t input number of look\n");
  fprintf(stderr, "\t verbose \t display progress bars (default 1)\n");
  fprintf(stderr, "\t hWmin \t\t radius of the smallest search window (default 1)\n");
  fprintf(stderr, "\t hWmax \t\t radius of the largest search window (default 12)\n");
  fprintf(stderr, "\t hPmin \t\t half width of the smallest patches (default 0)\n");
  fprintf(stderr, "\t hPmax \t\t half width of the largest patches (default 5)\n");
  fprintf(stderr, "\t simfunc \t similarity function to be used glr/kl/geo (default glr)\n");
  return 1;
}

int main(int argc, char* argv[])
{
  if (argc < 4)
    return usage(argv[0]);

  char* fn_in = argv[1];
  char* fn_out = argv[2];
  float L;
  int hWmin = 1, hWmax = 12, hPmin = 0, hPmax = 5, verbose = 1;
  int argi = 3;
  sardata* input;
  sarsimstats* output = NULL;
  char simfunc[1024] = "glr";

  if (sscanf(argv[argi++], "%f", &L) != 1)
    return usage(argv[0]);
  if (argc >= 5)
    if (sscanf(argv[argi++], "%d", &verbose) != 1)
      return usage(argv[0]);
  if (argc >= 6)
    if (sscanf(argv[argi++], "%d", &hWmin) != 1)
      return usage(argv[0]);
  if (argc >= 7)
    if (sscanf(argv[argi++], "%d", &hWmax) != 1)
      return usage(argv[0]);
  if (argc >= 8)
    if (sscanf(argv[argi++], "%d", &hPmin) != 1)
      return usage(argv[0]);
  if (argc >= 9)
    if (sscanf(argv[argi++], "%d", &hPmax) != 1)
      return usage(argv[0]);
  if (argc >= 10)
    sscanf(argv[argi++], "%s", simfunc);
  argi++;
  input = sardata_alloc();
  if (!(input = sarread(fn_in, input)))
    {
      sarerror_msg_msg("Cannot open file %s", fn_in);
      fprintf(stderr, "%s\n", sarerror);
      return 2;
    }
  if (!(output = sarnlstats(input, L, 6, verbose,
			    hWmin, hWmax, hPmin, hPmax,
			    simfunc)))
    {
      fprintf(stderr, "%s\n", sarerror);
      return 2;
    }
  if (!(sarnlstats_write(output, fn_out)))
    {
      sarerror_msg_msg("Cannot create file %s", fn_out);
      fprintf(stderr, "%s\n", sarerror);
      return 2;
    }
  sardata_free(input);
  sarnlstats_free(output);
  return 0;
}
