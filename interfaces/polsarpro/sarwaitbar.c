#include "tools/sarwaitbar.h"

void* (*sarwaitbar_open)(void) = &sarwaitbar_std_open;
int (*sarwaitbar_update)(void*, int percent) = &sarwaitbar_std_update;
int (*sarwaitbar_close)(void*) = &sarwaitbar_std_close;
