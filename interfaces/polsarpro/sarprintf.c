#include <stdarg.h>
#include "tools/sarprintf.h"

int (*sarprintf)(const char* format, ...) = &sarprintf_std;
int (*sarprintf_ret)(const char* format, ...) = &sarprintf_std_ret;
int (*sarprintf_warning)(const char* format, ...) = &sarprintf_std_warning;
int (*sarprintf_error)(const char* format, ...) = &sarprintf_std_error;
