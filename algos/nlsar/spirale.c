#include <stdlib.h>
#include <math.h>
#include "spirale.h"

# define PI (3.14159265358979311599796346854419)

void spirale(int hW, int2* shifts, int* lengths, int hStep)
{
  float cx, cy;
  int idx, k, s, x, y;
  char* mask;

  mask = calloc((2*hW+3)*(2*hW+3), sizeof(char));

  idx = 0;
  for (k = 0; k <= hW; ++k)
    {
      if (k > 5 && k % hStep != 0)
	{
	  lengths[k] = lengths[k-1];
	  continue;
	}
      if (k == 0)
	{
	  mask[(hW+2)*(2*hW+3)+(hW+2)] = 1;
	  shifts[idx][0] = 0;
	  shifts[idx][1] = 0;
	  idx = idx + 1;
	}
      else
	for (s = 0; s < 8*k; ++s)
	  {
	    cx = k * cosf(((float) s) / (8*k) * 2 * PI);
	    cy = k * sinf(((float) s) / (8*k) * 2 * PI);
	    x = roundf(cx);
	    y = roundf(cy);
	    if (!mask[(x+hW+2)*(2*hW+3)+(y+hW+2)] &&
		(k-0.5)*(k-0.5) < x*x+y*y && x*x+y*y <= (k+0.5)*(k+0.5))
	      {
		mask[(x+hW+2)*(2*hW+3)+(y+hW+2)] = 1;
		shifts[idx][0] = x;
		shifts[idx][1] = y;
		idx = idx + 1;
	      }
	    for (x = floorf(cx); x <= ceilf(cx); ++x)
	      for (y = floorf(cy); y <= ceilf(cy); ++y)
		{
		  if (!mask[(x+hW+2)*(2*hW+3)+(y+hW+2)] &&
		      (k-0.5)*(k-0.5) < x*x+y*y && x*x+y*y <= (k+0.5)*(k+0.5))
		    {
		      mask[(x+hW+2)*(2*hW+3)+(y+hW+2)] = 1;
		      shifts[idx][0] = x;
		      shifts[idx][1] = y;
		      idx = idx + 1;
		    }
		}
	  }
      if (k > 0)
      	idx = idx - (idx - lengths[k-1]) / 2;
      lengths[k] = idx;
    }
  if (hStep > 1 && hW >= 5-1)
    {
      for (idx = lengths[5-1]; idx < lengths[hW]; ++idx)
	{
	  x = shifts[idx][0] + rand() % (2*hStep+1) - hStep;
	  y = shifts[idx][1] + rand() % (2*hStep+1) - hStep;
	  if (0 <= (x+hW+2) && (x+hW+2) < (2*hW+3) &&
	      0 <= (y+hW+2) && (y+hW+2) < (2*hW+3) &&
	      !mask[(x+hW+2)*(2*hW+3)+(y+hW+2)])
	    {
	      mask[(x+hW+2)*(2*hW+3)+(y+hW+2)] = 1;
	      mask[(shifts[idx][0]+hW+2)*(2*hW+3)+(shifts[idx][1]+hW+2)] = 0;
	      shifts[idx][0] = x;
	      shifts[idx][1] = y;
	    }
	}
    }
  free(mask);
}

