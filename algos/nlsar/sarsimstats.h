/*
** sarsimstats.h: declarations for statistics of similarity criteria
**
** This file is part of NL-SAR Toolbox version 0.8.
**
** Copyright Charles-Alban Deledalle (2016)
** Email charles-alban.deledalle@math.u-bordeaux.fr
**
** This software is a computer program whose purpose is to provide a
** suite of tools to manipulate SAR images.
**
** This software is governed by the CeCILL license under French law and
** abiding by the rules of distribution of free software. You can use,
** modify and/ or redistribute the software under the terms of the CeCILL
** license as circulated by CEA, CNRS and INRIA at the following URL
** "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided only
** with a limited warranty and the software's author, the holder of the
** economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards their
** requirements in conditions enabling the security of their systems and/or
** data to be ensured and, more generally, to use and operate it in the
** same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL license and that you accept its terms.
**
**
** Started on  Wed Jul 24 15:42:05 2013 Charles-Alban Deledalle
** Last update Fri Jul 15 10:25:52 2016 Charles-Alban Deledalle
*/

#ifndef SARSIMSTATS_H_
# define SARSIMSTATS_H_

#include "data/sardata.h"
#include "sarsim.h"
#include "spirale.h"

typedef enum {
  simtype_glr,
  simtype_kl,
  simtype_geo
} sarsimtype;

typedef struct {
  int	N;
  float mean;
  float std;
  float min;
  float max;
  float selfsim;
  float q_min;
  float q_max;
  float q_step;
  float* alpha;
  float* quantile;
  int* val2idxsup;
  int* val2idxinf;
} sarsimstats_data;

typedef struct {
  sarsimtype simtype; // Type of criterion used to compared covariances
  int	hWmin;  // Smallest half search window size (radius)
  int	hWmax;  // Largest half search window size (radius)
  int	Wmax;   // Number of elements in the largest search window
  int	hPmin;  // Smallest half patch size (radius)
  int	hPmax;  // Smallest half patch size (radius)
  int	hStep;  // Half step size between two consecutive windows
  int	K;      // Number of different used patch sizes
  int	S;      // Number of different used patch sizes
  float L;      // Input ENL
  float eta2;   // Input CV
  float zeta2;  // Input CV of finite diff
  int	is_corr;// Indicates if noise is spatially correlated
  sarsimstats_data** data;
  int2* spirale_shifts;
  int*  spirale_lengths;
} sarsimstats;

#define SARSIMSTATS_ACCESS(stats, s, k, l) ((stats)->data[((s)*(stats)->K+(k))*((stats)->Wmax-1) + (l)])

sarsimstats*	sarnlstats(const sardata*		noise,
			   float			L,
			   int n_args, ...
			   // int verbose = 1,
			   // int hWmin = 1,
			   // int hWax = 12,
			   // int hPmin = 0,
			   // int hPmax = 5,
			   // char* simfunc = "glr",
			   );

sarsimstats*	sarnlstats_free(sarsimstats* stats);

sarsimstats*	sarnlstats_read(const char* filename);

int		sarnlstats_write(sarsimstats* stats, const char* filename);


#endif //!SARSIMSTATS_H_
