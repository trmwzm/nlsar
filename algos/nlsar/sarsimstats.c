/*
** sarsimstats.c: estimation of the statistis of similarity criteria
**
** This file is part of NL-SAR Toolbox version 0.8.
**
** Copyright Charles-Alban Deledalle (2016)
** Email charles-alban.deledalle@math.u-bordeaux.fr
**
** This software is a computer program whose purpose is to provide a
** suite of tools to manipulate SAR images.
**
** This software is governed by the CeCILL license under French law and
** abiding by the rules of distribution of free software. You can use,
** modify and/ or redistribute the software under the terms of the CeCILL
** license as circulated by CEA, CNRS and INRIA at the following URL
** "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided only
** with a limited warranty and the software's author, the holder of the
** economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards their
** requirements in conditions enabling the security of their systems and/or
** data to be ensured and, more generally, to use and operate it in the
** same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL license and that you accept its terms.
**
**
** Started on  Wed Jul 24 15:45:50 2013 Charles-Alban Deledalle
** Last update Tue Jul 19 16:58:11 2016 Charles-Alban Deledalle
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "data/sardata.h"
#include "tools/sarerror.h"
#include "tools/mathtools.h"
#include "tools/sarprintf.h"
#include "tools/sarwaitbar.h"
#include "algos/carfilter/carfilter.h"
#include "sarsim.h"
#include "sarsimstats.h"

static int compf(const void *p1, const void *p2)
{
  if ((* (float*) p1) > (* (float*) p2))
    return 1;
  if ((* (float*) p1) < (* (float*) p2))
    return -1;
  return 0;
}

static sardata* suppress_zero(sardata* data)
{
  int i, j, k, l;
  double min = INFINITY;
  double minp = INFINITY;
  double value;
  for (i = 0; i < data->M; ++i)
    for (j = 0; j < data->N; ++j)
      for (k = 0; k < data->D; ++k)
	{
	  value = cabsf(SARDATA_ACCESS(data, i, j, k, k));
	  if (value < min)
	    min = value;
	  if (0 < value && value < minp)
	    minp = value;
	}
  if (min == 0.0)
    for (i = 0; i < data->M; ++i)
      for (j = 0; j < data->N; ++j)
	for (k = 0; k < data->D; ++k)
	  if (cabsf(SARDATA_ACCESS(data, i, j, k, k)) == min)
	    {
	      for (l = 0; l < data->D; ++l)
		{
		  SARDATA_ACCESS(data, i, j, k, l) = 0;
		  SARDATA_ACCESS(data, i, j, l, k) = 0;
		}
	      SARDATA_ACCESS(data, i, j, k, k) = minp;
	    }
  return data;
}

static sarsimstats_data* sarnlstats_create_core(const sardata* input,
						const sarsimfuncs* simfunc,
						int dx, int dy,
						int L,
						int hP,
						float alpha_min,
						float alpha_max,
						int nb)
{
  sarsimstats_data* stats;
  int M = input->M;
  int N = input->N;
  int D = input->D;
  int Mext, Next;
  int x, y, x_dx, y_dy, k;
  float q_min, q_max, q;

  Mext = M+2*hP+1;
  Next = N+2*hP+1;

  if (!(0 <= alpha_min && alpha_min < alpha_max && alpha_max <= 1))
    {
      sarerror_msg("Quantiles sould verify 0 <= alpha_min < alpha_max <= 1");
      return NULL;
    }
  if (nb <= 1)
    {
      sarerror_msg("Number of bins should be at least 2");
      return NULL;
    }
  if (!(stats = calloc(1, sizeof(sarsimstats_data))))
    {
      sarerror_msg("Cannot allocate memory");
      return NULL;
    }
  stats->alpha = NULL;
  stats->quantile = NULL;
  stats->val2idxsup = NULL;
  stats->val2idxinf = NULL;
  stats->N = nb;
  float* diff_cum = malloc(Mext * Next * sizeof(float));
  float* diff = malloc(M * N * sizeof(float));
  if (!diff && !diff_cum)
    {
      sarerror_msg("Cannot allocate memory");
      return NULL;
    }
  sarsimdata* inputsarsim = simfunc->create(L, input);
  if (!inputsarsim)
    {
      sarerror_perror();
      return NULL;
    }
  float mean, m2, min, max;

  // Compute similarities
  for (x = -hP-1; x < M+hP; ++x)
    {
      for (y = -hP-1; y < N+hP; ++y)
	{
	  x_dx = MOD(x + dx, M);
	  y_dy = MOD(y + dy, N);
	  diff_cum[(x+1+hP) * Next + (y+1+hP)] =
	    simfunc->lsarsim(D,
	  		     SARSIMDATA_ACCESS(inputsarsim, MOD(x, M), MOD(y, N)),
	  		     SARSIMDATA_ACCESS(inputsarsim, x_dx, y_dy));
	}
    }
  // Compute commulative sums
  for (y = 1; y < Next; ++y)
    diff_cum[y] += diff_cum[y-1];
  for (x = 1; x < Mext; ++x)
    diff_cum[x * Next] += diff_cum[(x-1) * Next];
  for (x = 1; x < Mext; ++x)
    for (y = 1; y < Next; ++y)
      diff_cum[x * Next + y] +=
	+ diff_cum[(x-1) * Next + y]
	+ diff_cum[x     * Next + (y-1)]
	- diff_cum[(x-1) * Next + (y-1)];

  // Compute sums
  for (x = 0; x < M; ++x)
    for (y = 0; y < N; ++y)
      {
	x_dx = MOD(x + dx, M);
	y_dy = MOD(y + dy, N);

	diff[x * N + y] =
	  + diff_cum[(x+1+hP+hP)   * Next + (y+1+hP+hP)]
	  - diff_cum[(x+1+hP+hP)   * Next + (y+1+hP-hP-1)]
	  - diff_cum[(x+1+hP-hP-1) * Next + (y+1+hP+hP)]
	  + diff_cum[(x+1+hP-hP-1) * Next + (y+1+hP-hP-1)];
      }

  // Compute statistics
  stats->selfsim =
    simfunc->lsarsim(D,
		     SARSIMDATA_ACCESS(inputsarsim, MOD(dx, M), MOD(dy, N)),
		     SARSIMDATA_ACCESS(inputsarsim, MOD(dx, M), MOD(dy, N)));
  simfunc->free(inputsarsim);
  mean = diff[0];
  m2   = diff[0] * diff[0];
  min  = diff[0];
  max  = diff[0];
  for (k = 1; k < M * N; ++k)
    {
      mean += diff[k];
      m2   += diff[k] * diff[k];
      min   = MIN(diff[k], min);
      max   = MAX(diff[k], max);
    }
  mean /= M * N;
  m2 /= M * N;
  stats->mean = mean;
  stats->std = sqrtf(m2 - mean * mean);
  stats->min = min;
  stats->max = max;
  qsort(diff, M * N, sizeof(float), compf);
  q_min = diff[(int) ((M * N - 1) * alpha_min)];
  q_max = diff[(int) ((M * N - 1) * alpha_max)];
  stats->q_min = q_min;
  stats->q_max = q_max;
  if (!stats->alpha)
    if (!(stats->alpha = malloc(nb * sizeof(float))))
      {
	sarerror_perror();
	return NULL;
      }
  if (!stats->quantile)
    if (!(stats->quantile = malloc(nb * sizeof(float))))
      {
	sarerror_perror();
	return NULL;
      }
  if (!stats->val2idxsup)
    if (!(stats->val2idxsup = malloc(nb * sizeof(int))))
      {
	sarerror_perror();
	return NULL;
      }
  if (!stats->val2idxinf)
    if (!(stats->val2idxinf = malloc(nb * sizeof(int))))
      {
	sarerror_perror();
	return NULL;
      }
  float alpha;
  for (k = 0; k < nb; ++k)
    {
      alpha = alpha_min + k * (alpha_max - alpha_min) / (nb - 1);
      q = diff[(int) ((M * N - 1) * alpha)];
      stats->alpha[k] = alpha;
      stats->quantile[k] = q;
    }
  int l = 0;
  for (k = 0; k < nb; ++k)
    {
      q = q_min + (k+1) * (q_max - q_min) / (nb - 1);
      while (l < nb && stats->quantile[l] <= q)
	++l;
      if (l >= nb)
	l = nb - 1;
      stats->val2idxsup[k] = l;
    }
  for (k = nb - 1; k >= 0; --k)
    {
      q = q_min + (k-1) * (q_max - q_min) / (nb - 1);
      while (l >= 0 && stats->quantile[l] > q)
	--l;
      if (l < 0)
	l = 0;
      stats->val2idxinf[k] = l;
    }
  stats->q_step = (q_max - q_min) / (nb - 1);
  free(diff);
  free(diff_cum);

  return stats;
}

static sarsimstats* sarnlstats_create(const sardata* input,
				      sarsimtype simtype,
				      float L,
				      int verbose,
				      int hWmin, int hWmax,
				      int hPmin, int hPmax,
				      int S,
				      float alpha_min,
				      float alpha_max,
				      int nb)
{
  const sarsimfuncs* simfunc;
  sardata* binput;
  sarsimstats* stats;
  int2* spirale_shifts;
  int*  spirale_lengths;
  int x, y, dx, dy;
  int hStep;
  float eta2, zeta2, corr;
  float sum_span, sum_span2, frob2, tmp, tmp2;
  float complex ctmp;
  int Wmax, K;
  int s, k, l;
  int is_corr;
  int gl_cpt = 0;
  int percent;
  void* hwaitbar = NULL;

  switch (simtype)
    {
      case simtype_glr:
	simfunc = &sarsim_glrwishart;
	break;
      case simtype_kl:
	simfunc = &sarsim_klwishart;
	break;
      case simtype_geo:
	simfunc = &sarsim_geowishart;
	break;
      default:
	sarerror_msg("Unkown similarity criterion");
	return NULL;
    }
  if (input->M * input->N > 256 * 256)
    sarprintf_warning("The noise image is very large (comutation might be very long).\n"
		      "\tTypical sizes should be ranging from 32x32 to 128x128,\n"
		      "\tMake sure the image contains only noise without structures,\n"
		      "\tand it reflects the targeted speckle statistics.");

  { // Noise analysis
    sum_span = 0;
    sum_span2 = 0;
    for (x = 0; x < input->M; ++x)
      for (y = 0; y < input->N; ++y)
	{
	  tmp = 0;
	  for (k = 0; k < 1; ++k)
	    tmp += SARDATA_ACCESS(input, x, y, k, k);
	  sum_span  += tmp;
	  sum_span2 += tmp * tmp;
	}
    frob2 = 0;
    for (k = 0; k < 1; ++k)
      for (l = 0; l < 1; ++l)
	{
	  ctmp = 0;
	  for (x = 0; x < input->M; ++x)
	    for (y = 0; y < input->N; ++y)
	      ctmp += SARDATA_ACCESS(input, x, y, k, l);
	  ctmp /= input->M * input->N;
	  frob2 += cabsf(ctmp) * cabsf(ctmp);
	}
    sum_span  /= input->M * input->N;
    sum_span2 /= input->M * input->N;
    eta2 = sum_span2 - sum_span * sum_span;
    sum_span = 0;
    sum_span2 = 0;
    for (x = 0; x < input->M-1; ++x)
      for (y = 0; y < input->N; ++y)
	{
	  tmp  = 0;
	  tmp2 = 0;
	  for (k = 0; k < 1; ++k)
	    {
	      tmp  += SARDATA_ACCESS(input, x, y, k, k);
	      tmp2 += SARDATA_ACCESS(input, x+1, y, k, k);
	    }
	  sum_span  += tmp;
	  sum_span2 += tmp * tmp2;
	}
    sum_span  /= (input->M - 1) * input->N;
    sum_span2 /= (input->M - 1) * input->N;
    corr = fabsf(sum_span2 - sum_span * sum_span) / eta2;
    eta2 = eta2 / frob2;
    is_corr = corr > 0.2; // /eta2;
  }
  S = 3;
  {
    sum_span2 = 0;
    for (x = 0; x < input->M-1; ++x)
      for (y = 0; y < input->N; ++y)
	{
	  tmp = 0;
	  for (k = 0; k < 1; ++k)
	    tmp +=
	      SARDATA_ACCESS(input, x, y, k, k) -
	      SARDATA_ACCESS(input, x+1, y, k, k);
	  sum_span2 += tmp * tmp;
	}
    sum_span2 /= (input->M - 1) * input->N;
    zeta2 = sum_span2 / frob2;
  }
  if (fabsf(eta2 * L - 1) > 0.1)
    sarprintf_warning("Empirical eta2 deviates more than 10%% w.r.t theoretical value.\n"
		      "\tIs the theoretical number of look correct?\n"
		      "\tDoes the image of noise contain only noise? No structures?\n"
		      "\tIs the image of noise big enough?");
  if (is_corr)
    {
      hStep  = ceilf(corr / 0.2);
      hWmax  = floorf(hWmax * sqrtf(hStep) / hStep) * hStep;
      hPmax *= hStep;
      hWmin  = ceilf(hWmin * sqrtf(hStep) / hStep) * hStep;
      hPmin *= hStep;
    }
  else
    hStep = 1;
  K = hPmax-hPmin+1;
  if (K < 1)
    K = 1;
  Wmax = (2*hWmax+1)*(2*hWmax+1);
  spirale_shifts = calloc(Wmax, sizeof(int2));
  spirale_lengths = calloc(hWmax+1, sizeof(int));
  spirale(hWmax, spirale_shifts, spirale_lengths, hStep);
  Wmax = spirale_lengths[hWmax];

  if (is_corr)
    sarprintf_warning("Noise is significantly spatially correlated.\n"
		      "\tHalf search window size has been increased to %d-%d,\n"
		      "\tHalf patch size has been increased to %d-%d,\n"
		      "\tOnly 1 over %d windows will be considered.",
		      hWmin, hWmax, hPmin, hPmax, hStep);

  stats = calloc(1, sizeof(sarsimstats));
  stats->simtype = simtype;
  stats->hWmin = hWmin;
  stats->hWmax = hWmax;
  stats->Wmax  = Wmax;
  stats->hPmin = hPmin;
  stats->hPmax = hPmax;
  stats->hStep = hStep;
  stats->K = K;
  stats->S = S;
  stats->L = L;
  stats->eta2 = eta2;
  stats->zeta2 = zeta2;
  stats->is_corr = is_corr;
  stats->data = calloc(S * K * (Wmax-1), sizeof(sarsimstats_data*));
  stats->spirale_shifts = spirale_shifts;
  stats->spirale_lengths = spirale_lengths;

  if (verbose)
    {
      sarprintf("eta2=%.2f [theoretical=%.2f] zeta2=%.2f corr=%.2f\n",
		eta2, 1./L, zeta2, corr);
      sarprintf("=> #scales=%d, Wmax=%d\n", S, Wmax);
      hwaitbar = sarwaitbar_open();
    }
  binput = sardata_dup(input);
  for (s = 0; s < S; ++s)
    {
      if (s)
	binput = sargausscar(input, binput, s);
#pragma omp parallel default(shared) private(k,dx,dy,l)
      {
# pragma omp for schedule(dynamic) nowait
	for (k = 0; k < K; ++k)
	  for (l = 0; l < Wmax-1; ++l)
	    {
#pragma omp atomic
	      gl_cpt++;

	      dx = spirale_shifts[l+1][0];
	      dy = spirale_shifts[l+1][1];

	      stats->data[(s*K+k)*(Wmax-1)+l] =
		sarnlstats_create_core(binput, simfunc,
				       dx, dy, L, k+hPmin,
				       alpha_min, alpha_max, nb);
	      if (verbose)
#pragma omp critical
		{
		  percent = (100 * gl_cpt / S / K / (Wmax-1));
		  if (percent != (100 * (gl_cpt-1) / S / K / (Wmax-1)))
		    sarwaitbar_update(hwaitbar, percent);
		}
	    }
      }
    }
  if (verbose)
    {
      sarwaitbar_close(hwaitbar);
      for (s = 0; s < S; ++s)
	for (k = 0; k < K; k += MAX(1, K-1))
	  sarprintf("s=%d hP=%2d mean=%f std=%f\n",
		    s+1, k+hPmin,
		    stats->data[(s*K+k)*(Wmax-1)+Wmax-2]->mean / (2*(k+hPmin)+1) / (2*(k+hPmin)+1),
		    stats->data[(s*K+k)*(Wmax-1)+Wmax-2]->std / (2*(k+hPmin)+1) / (2*(k+hPmin)+1));
    }
  sardata_free(binput);
  return stats;
}

sarsimstats* sarnlstats(const sardata* noise,
			float L,
			int n_args,
			...)
{
  int hWmin = 1;
  int hWmax = 12;
  int hPmin = 0;
  int hPmax = 5;
  int S = 3;
  int verbose = 1;
  int i;
  va_list ap;
  sardata* bnoise;
  sarsimstats* stats;
  sarsimtype simtype;
  char *sim = "glr";

  va_start(ap, n_args);
  for (i = 0; i < n_args; i++)
    switch (i)
      {
	case 0:
	  verbose = va_arg(ap, int);
	  break;
	case 1:
	  hWmin = va_arg(ap, int);
	  break;
	case 2:
	  hWmax = va_arg(ap, int);
	  break;
	case 3:
	  hPmin = va_arg(ap, int);
	  break;
	case 4:
	  hPmax = va_arg(ap, int);
	  break;
	case 5:
	  sim = va_arg(ap, char*);
	  break;
	default:
	  sarerror_msg("Too many arguments");
	  return NULL;
      }
  va_end(ap);

  if (hWmin < 0 || hPmin < 0 || hWmin > hWmax || hPmin > hPmax)
    {
      sarerror_msg("Window sizes are inconsistent");
      return NULL;
    }

  if (!strcasecmp(sim, "glr"))
    simtype = simtype_glr;
  else
    if (!strcasecmp(sim, "kl"))
      simtype = simtype_kl;
    else
      if (!strcasecmp(sim, "geo"))
	simtype = simtype_geo;
      else
	{
	  sarerror_msg("Unkown similarity criterion");
	  return NULL;
	}
  if (verbose)
    sarprintf("Compute %s statistics\n", sim);

  bnoise = sardata_dup(noise);
  bnoise = suppress_zero(bnoise);
  if (!(stats = sarnlstats_create(bnoise, simtype, L, verbose,
				  hWmin, hWmax, hPmin, hPmax, S, 0.01, 0.99, 1024)))
    return NULL;

  sardata_free(bnoise);
  if (!stats)
    {
      sarerror_msg("Cannot precompute necessary stuffs");
      return NULL;
    }

  return stats;
}

sarsimstats* sarnlstats_free(sarsimstats* stats)
{
  int k;

  if (stats)
    {
      for (k = 0; k < stats->S * stats->K * (stats->Wmax - 1); ++k)
	{
	  if (stats->data[k]->quantile)
	    free(stats->data[k]->quantile);
	  if (stats->data[k]->alpha)
	    free(stats->data[k]->alpha);
	  if (stats->data[k]->val2idxsup)
	    free(stats->data[k]->val2idxsup);
	  if (stats->data[k]->val2idxinf)
	    free(stats->data[k]->val2idxinf);
	  free(stats->data[k]);
	}
      free(stats->data);
      free(stats->spirale_shifts);
      free(stats->spirale_lengths);
      free(stats);
    }
  return NULL;
}

sarsimstats* sarnlstats_read(const char* filename)
{
  FILE* f;
  sarsimstats* stats;
  int k;

  if (!(f = fopen(filename, "rb")))
    {
      sarerror_perror();
      return NULL;
    }
  stats = malloc(sizeof(sarsimstats));
  if (fread(stats, sizeof(sarsimstats), 1, f) != 1)
    {
      sarerror_msg("Invalid file");
      return NULL;
    }
  stats->data = malloc(stats->S * stats->K * (stats->Wmax - 1) * sizeof(sarsimstats_data*));
  for (k = 0; k < stats->S * stats->K * (stats->Wmax - 1); ++k)
    {
      stats->data[k] = malloc(sizeof(sarsimstats_data));
      if (fread(stats->data[k], sizeof(sarsimstats_data), 1, f) != 1)
	{
	  sarerror_msg("Invalid file");
	  return NULL;
	}
      stats->data[k]->alpha = malloc(stats->data[k]->N * sizeof(float));
      if ((int) fread(stats->data[k]->alpha, sizeof(float), stats->data[k]->N, f) != stats->data[k]->N)
	{
	  sarerror_msg("Invalid file");
	  return NULL;
	}
      stats->data[k]->quantile = malloc(stats->data[k]->N * sizeof(float));
      if ((int) fread(stats->data[k]->quantile, sizeof(float), stats->data[k]->N, f) != stats->data[k]->N)
	{
	  sarerror_msg("Invalid file");
	  return NULL;
	}
      stats->data[k]->val2idxsup = malloc(stats->data[k]->N * sizeof(float));
      if ((int) fread(stats->data[k]->val2idxsup, sizeof(float), stats->data[k]->N, f) != stats->data[k]->N)
	{
	  sarerror_msg("Invalid file");
	  return NULL;
	}
      stats->data[k]->val2idxinf = malloc(stats->data[k]->N * sizeof(float));
      if ((int) fread(stats->data[k]->val2idxinf, sizeof(float), stats->data[k]->N, f) != stats->data[k]->N)
	{
	  sarerror_msg("Invalid file");
	  return NULL;
	}
    }
  stats->spirale_shifts = malloc(stats->Wmax * sizeof(int2));
  if ((int) fread(stats->spirale_shifts, sizeof(int2), stats->Wmax, f) != stats->Wmax)
    {
      sarerror_msg("Invalid file");
      return NULL;
    }
  stats->spirale_lengths = malloc(stats->Wmax * sizeof(int));
  if ((int) fread(stats->spirale_lengths, sizeof(int), stats->hWmax+1, f) != stats->hWmax + 1)
    {
      sarerror_msg("Invalid file");
      return NULL;
    }
  fclose(f);
  return stats;
}

int sarnlstats_write(sarsimstats* stats, const char* filename)
{
  FILE* f;
  int k;

  if (stats)
    {
      if (!(f = fopen(filename, "wb")))
	{
	  sarerror_perror();
	  return 0;
	}
      fwrite(stats, sizeof(sarsimstats), 1, f);
      for (k = 0; k < stats->S * stats->K * (stats->Wmax - 1); ++k)
	{
	  fwrite(stats->data[k], sizeof(sarsimstats_data), 1, f);
	  fwrite(stats->data[k]->alpha, sizeof(float), stats->data[k]->N, f);
	  fwrite(stats->data[k]->quantile, sizeof(float), stats->data[k]->N, f);
	  fwrite(stats->data[k]->val2idxsup, sizeof(float), stats->data[k]->N, f);
	  fwrite(stats->data[k]->val2idxinf, sizeof(float), stats->data[k]->N, f);
	}
      fwrite(stats->spirale_shifts, sizeof(int2), stats->Wmax, f);
      fwrite(stats->spirale_lengths, sizeof(int), stats->hWmax+1, f);
      fclose(f);
      return 1;
    }
  return 0;
}
