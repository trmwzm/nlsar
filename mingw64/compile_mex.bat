@echo on

@set MATLABROOT=C:\Program Files\MATLAB\R2013a

cd ..\interfaces\mex

gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I..\.. -I"%MATLABROOT%\extern\include" -DOMP -fopenmp  -c sarprintf.c -o sarprintf.o
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I..\.. -I"%MATLABROOT%\extern\include" -DOMP -fopenmp  -c sarwaitbar.c -o sarwaitbar.o

gcc -m64 -shared -I..\.. -I"%MATLABROOT%\extern\include" -o sar2rgb.mexw64 -DMATLAB_MEX_FILE -Wl,--export-all-symbols sarprintf.o sarwaitbar.o sar2rgb.c -L"%MATLABROOT%\bin\win64" -L"%MATLABROOT%\extern\lib\win64\microsoft" -L..\..\lib -lnlsartoolbox_weak -lmex -lmx -leng -L..\..\mingw64 -lblaslapack -lgfortran -DOMP -fopenmp

gcc -m64 -shared -I..\.. -I"%MATLABROOT%\extern\include" -o sarboxcar.mexw64 -DMATLAB_MEX_FILE -Wl,--export-all-symbols sarprintf.o sarwaitbar.o sarboxcar.c -L"%MATLABROOT%\bin\win64" -L"%MATLABROOT%\extern\lib\win64\microsoft" -L..\..\lib -lnlsartoolbox_weak -lmex -lmx -leng -L..\..\mingw64 -lblaslapack -lgfortran -DOMP -fopenmp

gcc -m64 -shared -I..\.. -I"%MATLABROOT%\extern\include" -o sardiskcar.mexw64 -DMATLAB_MEX_FILE -Wl,--export-all-symbols sarprintf.o sarwaitbar.o sardiskcar.c -L"%MATLABROOT%\bin\win64" -L"%MATLABROOT%\extern\lib\win64\microsoft" -L..\..\lib -lnlsartoolbox_weak -lmex -lmx -leng -L..\..\mingw64 -lblaslapack -lgfortran -DOMP -fopenmp

gcc -m64 -shared -I..\.. -I"%MATLABROOT%\extern\include" -o sargausscar.mexw64 -DMATLAB_MEX_FILE -Wl,--export-all-symbols sarprintf.o sarwaitbar.o sargausscar.c -L"%MATLABROOT%\bin\win64" -L"%MATLABROOT%\extern\lib\win64\microsoft" -L..\..\lib -lnlsartoolbox_weak -lmex -lmx -leng -L..\..\mingw64 -lblaslapack -lgfortran -DOMP -fopenmp

gcc -m64 -shared -I..\.. -I"%MATLABROOT%\extern\include" -o sargetweights.mexw64 -DMATLAB_MEX_FILE -Wl,--export-all-symbols sarprintf.o sarwaitbar.o sargetweights.c -L"%MATLABROOT%\bin\win64" -L"%MATLABROOT%\extern\lib\win64\microsoft" -L..\..\lib -lnlsartoolbox_weak -lmex -lmx -leng -L..\..\mingw64 -lblaslapack -lgfortran -DOMP -fopenmp

gcc -m64 -shared -I..\.. -I"%MATLABROOT%\extern\include" -o sarhomogeneous.mexw64 -DMATLAB_MEX_FILE -Wl,--export-all-symbols sarprintf.o sarwaitbar.o sarhomogeneous.c -L"%MATLABROOT%\bin\win64" -L"%MATLABROOT%\extern\lib\win64\microsoft" -L..\..\lib -lnlsartoolbox_weak -lmex -lmx -leng -L..\..\mingw64 -lblaslapack -lgfortran -DOMP -fopenmp

gcc -m64 -shared -I..\.. -I"%MATLABROOT%\extern\include" -o sarinfo.mexw64 -DMATLAB_MEX_FILE -Wl,--export-all-symbols sarprintf.o sarwaitbar.o sarinfo.c -L"%MATLABROOT%\bin\win64" -L"%MATLABROOT%\extern\lib\win64\microsoft" -L..\..\lib -lnlsartoolbox_weak -lmex -lmx -leng -L..\..\mingw64 -lblaslapack -lgfortran -DOMP -fopenmp

gcc -m64 -shared -I..\.. -I"%MATLABROOT%\extern\include" -o sarjoin.mexw64 -DMATLAB_MEX_FILE -Wl,--export-all-symbols sarprintf.o sarwaitbar.o sarjoin.c -L"%MATLABROOT%\bin\win64" -L"%MATLABROOT%\extern\lib\win64\microsoft" -L..\..\lib -lnlsartoolbox_weak -lmex -lmx -leng -L..\..\mingw64 -lblaslapack -lgfortran -DOMP -fopenmp

gcc -m64 -shared -I..\.. -I"%MATLABROOT%\extern\include" -o sarnlsar.mexw64 -DMATLAB_MEX_FILE -Wl,--export-all-symbols sarprintf.o sarwaitbar.o sarnlsar.c -L"%MATLABROOT%\bin\win64" -L"%MATLABROOT%\extern\lib\win64\microsoft" -L..\..\lib -lnlsartoolbox_weak -lmex -lmx -leng -L..\..\mingw64 -lblaslapack -lgfortran -DOMP -fopenmp

gcc -m64 -shared -I..\.. -I"%MATLABROOT%\extern\include" -o sarnlstats.mexw64 -DMATLAB_MEX_FILE -Wl,--export-all-symbols sarprintf.o sarwaitbar.o sarnlstats.c -L"%MATLABROOT%\bin\win64" -L"%MATLABROOT%\extern\lib\win64\microsoft" -L..\..\lib -lnlsartoolbox_weak -lmex -lmx -leng -L..\..\mingw64 -lblaslapack -lgfortran -DOMP -fopenmp

gcc -m64 -shared -I..\.. -I"%MATLABROOT%\extern\include" -o sarnlstats_free.mexw64 -DMATLAB_MEX_FILE -Wl,--export-all-symbols sarprintf.o sarwaitbar.o sarnlstats_free.c -L"%MATLABROOT%\bin\win64" -L"%MATLABROOT%\extern\lib\win64\microsoft" -L..\..\lib -lnlsartoolbox_weak -lmex -lmx -leng -L..\..\mingw64 -lblaslapack -lgfortran -DOMP -fopenmp

gcc -m64 -shared -I..\.. -I"%MATLABROOT%\extern\include" -o sarnlstats_read.mexw64 -DMATLAB_MEX_FILE -Wl,--export-all-symbols sarprintf.o sarwaitbar.o sarnlstats_read.c -L"%MATLABROOT%\bin\win64" -L"%MATLABROOT%\extern\lib\win64\microsoft" -L..\..\lib -lnlsartoolbox_weak -lmex -lmx -leng -L..\..\mingw64 -lblaslapack -lgfortran -DOMP -fopenmp

gcc -m64 -shared -I..\.. -I"%MATLABROOT%\extern\include" -o sarnlstats_write.mexw64 -DMATLAB_MEX_FILE -Wl,--export-all-symbols sarprintf.o sarwaitbar.o sarnlstats_write.c -L"%MATLABROOT%\bin\win64" -L"%MATLABROOT%\extern\lib\win64\microsoft" -L..\..\lib -lnlsartoolbox_weak -lmex -lmx -leng -L..\..\mingw64 -lblaslapack -lgfortran -DOMP -fopenmp

gcc -m64 -shared -I..\.. -I"%MATLABROOT%\extern\include" -o sarwrite.mexw64 -DMATLAB_MEX_FILE -Wl,--export-all-symbols sarprintf.o sarwaitbar.o sarwrite.c -L"%MATLABROOT%\bin\win64" -L"%MATLABROOT%\extern\lib\win64\microsoft" -L..\..\lib -lnlsartoolbox_weak -lmex -lmx -leng -L..\..\mingw64 -lblaslapack -lgfortran -DOMP -fopenmp

gcc -m64 -shared -I..\.. -I"%MATLABROOT%\extern\include" -o sarread.mexw64 -DMATLAB_MEX_FILE -Wl,--export-all-symbols sarprintf.o sarwaitbar.o sarread.c -L"%MATLABROOT%\bin\win64" -L"%MATLABROOT%\extern\lib\win64\microsoft" -L..\..\lib -lnlsartoolbox_weak -lmex -lmx -leng -L..\..\mingw64 -lblaslapack -lgfortran -DOMP -fopenmp

cd ..\..\mingw64
