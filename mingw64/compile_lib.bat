@echo on

cd ..

gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I. -DOMP -fopenmp  -c interfaces/lib/sarprintf.c -o interfaces/lib/sarprintf.o
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I. -DOMP -fopenmp  -c interfaces/lib/sarwaitbar.c -o interfaces/lib/sarwaitbar.o

md lib 2> NUL
ar cr lib/libnlsartoolbox.a tools/sarerror.o tools/sarprintf.o tools/sarwaitbar.o tools/matrixtools.o tools/inv_func.o data/iorat.o data/iobin.o data/ioxima.o data/iosar.o data/ionetpbm.o data/sardata.o data/rgbdata.o data/fltdata.o data/sar2rgb.o algos/carfilter/sarboxcar.o algos/carfilter/sardiskcar.o algos/carfilter/sargausscar.o algos/carfilter/fltboxcar.o algos/carfilter/fltdilate.o algos/nlsar/nlsar.o algos/nlsar/sarsimstats.o algos/nlsar/sarsim_glrwishart.o algos/nlsar/sarsim_klwishart.o algos/nlsar/sarsim_geowishart.o algos/nlsar/phi.o algos/nlsar/spirale.o algos/nlsar/getweights.o algos/noisegen/noisegen.o algos/noiseest/noiseest.o interfaces/lib/sarprintf.o interfaces/lib/sarwaitbar.o
ranlib lib/libnlsartoolbox.a
ar cr lib/libnlsartoolbox_weak.a tools/sarerror.o tools/sarprintf.o tools/sarwaitbar.o tools/matrixtools.o tools/inv_func.o data/iorat.o data/iobin.o data/ioxima.o data/iosar.o data/ionetpbm.o data/sardata.o data/rgbdata.o data/fltdata.o data/sar2rgb.o algos/carfilter/sarboxcar.o algos/carfilter/sardiskcar.o algos/carfilter/sargausscar.o algos/carfilter/fltboxcar.o algos/carfilter/fltdilate.o algos/nlsar/nlsar.o algos/nlsar/sarsimstats.o algos/nlsar/sarsim_glrwishart.o algos/nlsar/sarsim_klwishart.o algos/nlsar/sarsim_geowishart.o algos/nlsar/phi.o algos/nlsar/spirale.o algos/nlsar/getweights.o algos/noisegen/noisegen.o algos/noiseest/noiseest.o
ranlib lib/libnlsartoolbox_weak.a
gcc -shared -o lib/libnlsartoolbox.dll tools/sarerror.o tools/sarprintf.o tools/sarwaitbar.o tools/matrixtools.o tools/inv_func.o data/iorat.o data/iobin.o data/ioxima.o data/iosar.o data/ionetpbm.o data/sardata.o data/rgbdata.o data/fltdata.o data/sar2rgb.o algos/carfilter/sarboxcar.o algos/carfilter/sardiskcar.o algos/carfilter/sargausscar.o algos/carfilter/fltboxcar.o algos/carfilter/fltdilate.o algos/nlsar/nlsar.o algos/nlsar/sarsimstats.o algos/nlsar/sarsim_glrwishart.o algos/nlsar/sarsim_klwishart.o algos/nlsar/sarsim_geowishart.o algos/nlsar/phi.o algos/nlsar/spirale.o algos/nlsar/getweights.o algos/noisegen/noisegen.o algos/noiseest/noiseest.o interfaces/lib/sarprintf.o interfaces/lib/sarwaitbar.o -lblaslapack -lgfortran -fopenmp -L./mingw64/

cd mingw64
