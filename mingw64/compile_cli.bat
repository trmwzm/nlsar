@echo on

cd ..\interfaces\cli

gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I../../ -DOMP -fopenmp  -c sarnlsar.c -o sarnlsar.o
gcc -o sarnlsar  sarnlsar.o -lnlsartoolbox -lblaslapack -lgfortran -L../../lib  -lm -lpthread -L../../mingw64/ -fopenmp
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I../../ -DOMP -fopenmp  -c sarnlstats.c -o sarnlstats.o
gcc -o sarnlstats  sarnlstats.o -lnlsartoolbox -lblaslapack -lgfortran -L../../lib  -lm -lpthread -L../../mingw64/ -fopenmp
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I../../ -DOMP -fopenmp  -c sargetweights.c -o sargetweights.o
gcc -o sargetweights  sargetweights.o -lnlsartoolbox -lblaslapack -lgfortran -L../../lib  -lm -lpthread -L../../mingw64/ -fopenmp
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I../../ -DOMP -fopenmp  -c sar2ppm.c -o sar2ppm.o
gcc -o sar2ppm  sar2ppm.o -lnlsartoolbox -lblaslapack -lgfortran -L../../lib  -lm -lpthread -L../../mingw64/ -fopenmp
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I../../ -DOMP -fopenmp  -c sarconvert.c -o sarconvert.o
gcc -o sarconvert  sarconvert.o -lnlsartoolbox -lblaslapack -lgfortran -L../../lib  -lm -lpthread -L../../mingw64/ -fopenmp
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I../../ -DOMP -fopenmp  -c sarinfo.c -o sarinfo.o
gcc -o sarinfo  sarinfo.o -lnlsartoolbox -lblaslapack -lgfortran -L../../lib  -lm -lpthread -L../../mingw64/ -fopenmp
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I../../ -DOMP -fopenmp  -c sarextract.c -o sarextract.o
gcc -o sarextract  sarextract.o -lnlsartoolbox -lblaslapack -lgfortran -L../../lib  -lm -lpthread -L../../mingw64/ -fopenmp
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I../../ -DOMP -fopenmp  -c sarhist.c -o sarhist.o
gcc -o sarhist  sarhist.o -lnlsartoolbox -lblaslapack -lgfortran -L../../lib  -lm -lpthread -L../../mingw64/ -fopenmp
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I../../ -DOMP -fopenmp  -c sarnoise.c -o sarnoise.o
gcc -o sarnoise  sarnoise.o -lnlsartoolbox -lblaslapack -lgfortran -L../../lib  -lm -lpthread -L../../mingw64/ -fopenmp
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I../../ -DOMP -fopenmp  -c sarmire.c -o sarmire.o
gcc -o sarmire  sarmire.o -lnlsartoolbox -lblaslapack -lgfortran -L../../lib  -lm -lpthread -L../../mingw64/ -fopenmp
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I../../ -DOMP -fopenmp  -c sarstats.c -o sarstats.o
gcc -o sarstats  sarstats.o -lnlsartoolbox -lblaslapack -lgfortran -L../../lib  -lm -lpthread -L../../mingw64/ -fopenmp
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I../../ -DOMP -fopenmp  -c sarboxcar.c -o sarboxcar.o
gcc -o sarboxcar  sarboxcar.o -lnlsartoolbox -lblaslapack -lgfortran -L../../lib  -lm -lpthread -L../../mingw64/ -fopenmp
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I../../ -DOMP -fopenmp  -c sardiskcar.c -o sardiskcar.o
gcc -o sardiskcar  sardiskcar.o -lnlsartoolbox -lblaslapack -lgfortran -L../../lib  -lm -lpthread -L../../mingw64/ -fopenmp
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I../../ -DOMP -fopenmp  -c sargausscar.c -o sargausscar.o
gcc -o sargausscar  sargausscar.o -lnlsartoolbox -lblaslapack -lgfortran -L../../lib  -lm -lpthread -L../../mingw64/ -fopenmp
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I../../ -DOMP -fopenmp  -c sarjoin.c -o sarjoin.o
gcc -o sarjoin  sarjoin.o -lnlsartoolbox -lblaslapack -lgfortran -L../../lib  -lm -lpthread -L../../mingw64/ -fopenmp
gcc  -O3 -W -Wall -g -fexceptions -DBLAS -DLAPACK -I../../ -DOMP -fopenmp  -c sarcat.c -o sarcat.o
gcc -o sarcat  sarcat.o -lnlsartoolbox -lblaslapack -lgfortran -L../../lib  -lm -lpthread -L../../mingw64/ -fopenmp

cd ..\..\mingw64
