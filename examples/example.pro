print, sarinfo('example.rat')

sarimage = sarread('file.rat')

sarimage_box = sarboxcar(sarimage)
sarimage_disk = sarboxcar(sarimage)
sarimage_gauss = sarboxcar(sarimage)
[sarimage_box, eqlook_nl] = sarboxcar(sarimage, 3)

sarshow, sarimage
sarshow, sarimage_nl
tvslc, eqlook_nl
sarshow, sarimage_box
sarshow, sarimage_disk
sarshow, sarimage_gauss


