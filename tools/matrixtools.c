/*
** matrixtools.c: implementation of matrix tools
**
** This file is part of NL-SAR Toolbox version 0.8.
**
** Copyright Charles-Alban Deledalle (2016)
** Email charles-alban.deledalle@math.u-bordeaux.fr
**
** This software is a computer program whose purpose is to provide a
** suite of tools to manipulate SAR images.
**
** This software is governed by the CeCILL license under French law and
** abiding by the rules of distribution of free software. You can use,
** modify and/ or redistribute the software under the terms of the CeCILL
** license as circulated by CEA, CNRS and INRIA at the following URL
** "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided only
** with a limited warranty and the software's author, the holder of the
** economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards their
** requirements in conditions enabling the security of their systems and/or
** data to be ensured and, more generally, to use and operate it in the
** same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL license and that you accept its terms.
**
**
** Started on  Wed Jul 24 16:07:53 2013 Charles-Alban Deledalle
** Last update Tue Jul 19 16:25:43 2016 Charles-Alban Deledalle
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <complex.h>
#include "mathtools.h"
#include "matrixtools.h"
#include "sarprintf.h"


float trace(int D, const float complex* C)
{
  float res = 0;
  int k;
  for (k = 0; k < D; ++k)
    res += cabsf(MATRIX_ACCESS(C, D, k, k));
  return res;
}

float det(int D, const float complex* C)
{
#ifdef LAPACK
  int k, l;
  float complex* CHO_C;
  long int info, longD = D;
  float res;
#endif //LAPACK
  switch (D)
    {
      case 1:
	return (MATRIX_ACCESS(C, D, 0, 0));
      case 2:
	return
	  + (MATRIX_ACCESS(C, D, 0, 0)) * (MATRIX_ACCESS(C, D, 1, 1))
	  - CABS2(MATRIX_ACCESS(C, D, 1, 0));
      case 3:
      	return
      	  + (MATRIX_ACCESS(C, D, 0, 0)) * (MATRIX_ACCESS(C, D, 1, 1)) * (MATRIX_ACCESS(C, D, 2, 2))
      	  - (MATRIX_ACCESS(C, D, 0, 0)) * CABS2(MATRIX_ACCESS(C, D, 1, 2))
      	  - (MATRIX_ACCESS(C, D, 1, 1)) * CABS2(MATRIX_ACCESS(C, D, 0, 2))
      	  - (MATRIX_ACCESS(C, D, 2, 2)) * CABS2(MATRIX_ACCESS(C, D, 0, 1))
      	  + 2 * crealf(MATRIX_ACCESS(C, D, 0, 1) * MATRIX_ACCESS(C, D, 1, 2) * MATRIX_ACCESS(C, D, 2, 0));
      default:
#ifdef LAPACK
	// det(C) = Prod(k) | L(k, k) |^2
	// with L the cholesky lower matrix of C
	CHO_C = malloc(D * D * sizeof(float complex));
	for (k = 0; k < D; ++k)
	  for (l = k; l < D; ++l)
	    CHO_C[k * D + l] = MATRIX_ACCESS(C, D, k, l);
	cpotrf_("L", &longD, CHO_C, &longD, &info);
	res = 1;
	for (k = 0; k < D; ++k)
	  res *= CABS2(CHO_C[k * D + k]);
	free(CHO_C);
	return res;
#else //LAPACK
	sarprintf_std_error("Error: Determinant for matrix of dimension %d required LAPACK to be installed.\n", D);
	sarprintf_std_error("       Please install CLAPACK, configure and recompile\n");
	exit(3);
	return 1;
#endif //LAPACK
    }
}

float complex trace_invC1_C2(int D,
			     const float complex* C1,
			     const float complex* C2)
{
  switch (D)
    {
      case 1:
      	return crealf(MATRIX_ACCESS(C2, D, 0, 0)) / crealf(MATRIX_ACCESS(C1, D, 0, 0));
      case 2:
      	return
      	  (+ crealf(MATRIX_ACCESS(C1, D, 1, 1)) * crealf(MATRIX_ACCESS(C2, D, 0, 0))
      	   - 2 * crealf(MATRIX_ACCESS(C1, D, 0, 1) * MATRIX_ACCESS(C2, D, 1, 0))
      	   + crealf(MATRIX_ACCESS(C1, D, 0, 0)) * crealf(MATRIX_ACCESS(C2, D, 1, 1))) /
      	  det(D, C1);
      case 3:
      	return
      	  ((crealf(MATRIX_ACCESS(C1, D, 1, 1)) * crealf(MATRIX_ACCESS(C1, D, 2, 2))
      	    - CABS2(MATRIX_ACCESS(C1, D, 1, 2)))
      	   * crealf(MATRIX_ACCESS(C2, D, 0, 0))
      	   +
      	   2 * crealf((MATRIX_ACCESS(C1, D, 0, 2) * MATRIX_ACCESS(C1, D, 2, 1)
      		       - MATRIX_ACCESS(C1, D, 0, 1) * MATRIX_ACCESS(C1, D, 2, 2))
      		      * MATRIX_ACCESS(C2, D, 1, 0))
      	   +
      	   2 * crealf((MATRIX_ACCESS(C1, D, 0, 1) * MATRIX_ACCESS(C1, D, 1, 2)
      		       - MATRIX_ACCESS(C1, D, 0, 2) * MATRIX_ACCESS(C1, D, 1, 1))
      		      * MATRIX_ACCESS(C2, D, 2, 0))
      	   +
      	   (crealf(MATRIX_ACCESS(C1, D, 0, 0)) * crealf(MATRIX_ACCESS(C1, D, 2, 2))
      	    - CABS2(MATRIX_ACCESS(C1, D, 0, 2)))
      	   * crealf(MATRIX_ACCESS(C2, D, 1, 1))
      	   +
      	   2 * crealf((MATRIX_ACCESS(C1, D, 0, 2) * MATRIX_ACCESS(C1, D, 1, 0)
      		       - MATRIX_ACCESS(C1, D, 0, 0) * MATRIX_ACCESS(C1, D, 1, 2))
      		      * MATRIX_ACCESS(C2, D, 2, 1))
      	   +
      	   (crealf(MATRIX_ACCESS(C1, D, 0, 0)) * crealf(MATRIX_ACCESS(C1, D, 1, 1))
      	    - CABS2(MATRIX_ACCESS(C1, D, 1, 0)))
      	   * crealf(MATRIX_ACCESS(C2, D, 2, 2))) /
      	  det(D, C1);
      default:
	sarprintf_std_error("Calculus for matrix of dimension %d is not implemented yet.\n", D);
	exit(3);
	return 1;
    }
}

float complex trace_C1_C2(int D,
			  const float complex* C1,
			  const float complex* C2)
{
  float complex res;
  int k, l;

  switch (D)
    {
      case 1:
      	return crealf(MATRIX_ACCESS(C1, D, 0, 0)) * crealf(MATRIX_ACCESS(C2, D, 0, 0));
      default:
	res = 0;
	for (k = 0; k < D; ++k)
	  for (l = 0; l < D; ++l)
	    res += crealf(MATRIX_ACCESS(C1, D, k, l)) * crealf(MATRIX_ACCESS(C2, D, l, k));
      	return res;
    }
}

float norm2_logspectrum_C1_C2(int D,
			      const float complex* C1,
			      const float complex* C2)
{
  float res;
#if defined(BLAS) && defined(LAPACK)
  int k;
  float complex C1_C2[144];
  float complex E_C1_C2[12];
  float complex work[24];
  float rwork[24];
  long int info, longD = D;
  long int lwork, ldv;
  float complex alpha = 1, beta = 0;
#endif //BLAS && LAPACK
  switch (D)
    {
      case 1:
	res = logf(cabsf(MATRIX_ACCESS(C1, D, 0, 0) * MATRIX_ACCESS(C2, D, 0, 0)));
	return res * res;
      default:
#if defined(BLAS) && defined(LAPACK)
	if (D > 12)
	  {
	    sarprintf_std_error("Calculus for matrix of dimension %d is not implemented yet\n");
	    exit(3);
	  }
	/* chemm_("L", "L", &longD, &longD, &alpha, */
	/*        C2, &longD, C1, &longD, &beta, C1_C2, &longD); */
	cgemm_("N", "N", &longD, &longD, &longD, &alpha,
	       C2, &longD, C1, &longD, &beta, C1_C2, &longD);
	lwork = 2 * longD;
	ldv = 1;
	cgeev_("N", "N", &longD, C1_C2, &longD,
	       E_C1_C2, NULL, &ldv, NULL, &ldv,
	       work, &lwork, rwork, &info);
	for (k = 0; k < D; ++k)
	  E_C1_C2[k] = logf(crealf(E_C1_C2[k]));
	res = 0;
	for (k = 0; k < D; ++k)
	  res += E_C1_C2[k] * E_C1_C2[k];
	return res;
#else //BLAS && LAPACK
	sarprintf_std_error("Calculus for matrix of dimension %d required BLAS and LAPACK to be installed.\n", D);
	sarprintf_std_error("Please install LAPACK, CBLAS, configure and recompile\n");
	exit(3);
	return 1;
#endif //BLAS && LAPACK
    }
}
